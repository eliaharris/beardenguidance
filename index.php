<?php

/* Include the Bearden file */

include_once "BeardenGuidance.php";

/* Create a new Bearden Object */

$Bearden = new BeardenGuidance();

/* Check if the requested action is null  */

$Bearden->getBeardenActionHandler ()->getRequestedAction () !== NULL

    /* Execute the requested action */

    ? $Bearden->getBeardenActionHandler ()->executeRequestedAction ( $Bearden->getBeardenActionHandler ()->getRequestedAction () )

    /* There was no action requested, do nothing */

    : FALSE;


?>

<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html class="ie ie6" lang="en"><![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7" lang="en"><![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"><!--<![endif]-->
<head>

    <!-- Basic Page Needs
     ================================================== -->
    <meta charset="utf-8">
    <title>Counselor Login</title>

    <!-- Mobile Specific
     ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
     ================================================== -->
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <!-- Java Script
     ================================================== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script src="js/flexslider.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/ender.min.js"></script>
    <script src="js/selectnav.js"></script>
    <script src="js/imagebox.min.js"></script>
    <script src="js/carousel.js"></script>
    <script src="js/twitter.js"></script>
    <script src="js/tooltip.js"></script>
    <script src="js/popover.js"></script>
    <script src="js/effects.js"></script>

</head>
<body>

<!-- Header -->
<div id="header">

    <!-- 960 Container Start -->
    <div class="container ie-dropdown-fix">

        <!-- Logo -->
        <div class="four columns">

        </div>

        <!-- Main Navigation Start -->
        <div class="twelve columns">
            <div id="navigation">
                <ul id="nav">
                    <li><a id="current" href="/index">Counselor Login</a></li>

                    <li><a href="/takeForm">Take Form </a></li>


                </ul>
            </div>
        </div>
        <!-- Main Navigation End -->

    </div>
    <!-- 960 Container End -->

</div>
<!-- End Header -->

<div id="page-title">

    <!-- 960 Container Start -->
    <div class="container">
        <div class="sixteen columns">
            <h2>Counselor Login</h2>
        </div>
    </div>
    <!-- 960 Container End -->

</div>

<!-- 960 Container Start -->
</br>
<div class="container">
    <center>
        <div class="sixteen columns">
            <div class="large-notice" style="width: 50%;">
                <img src="images/logo.png"/>

                <form action="/index" method="post">
                    <table>

                        <tr>
                            <td><label>Username:</label><input type="text" name="username" size="40"></td>
                        </tr>

                        <tr>
                            <td><label>Password:</label><input type="password" name="password"
                                                               size="40"></td>
                        </tr>

                        </tr>
                        <tr>
                            <td>
                                <input type="submit" value="Login" class="button medium blue">
                            </td>
                        </tr>
                        <input type="hidden" name="action" value="login"/>
                        <input type="hidden" name="location"
                               value="<?php echo $Bearden->getBeardenUser ()->getLocation (); ?>"


                        </tr>

                    </table>
                </form>
                <?

                /*  Check if the message is empty */

                if ( !empty( $_GET[ 'message' ] ) ) {

                    /*  Check if the message is GREEN */

                    echo isset( $_GET[ 'green' ] )

                        /* The message is green, make the HTML markup according  */

                        ? '<div class="notification success closeable">'

                        /* The message is green, make the HTML markup according  */

                        : '<div class="notification error closeable">';

                    /*  Now echo the proper message */

                    echo '<p><span>' . $_GET[ 'message' ] . '</span></p>';

                    /*  End the div for the message box */

                    echo '</div>';
                }
                ?>
            </div>


        </div>
    </center>
</div>
<div id="footer">

    <!-- 960 Container -->
    <div class="container">

        <!-- About -->


        <!-- Twitter -->


    </div>
    <!-- 960 Container End -->

</div>
<!-- Footer End -->

<!--  Footer Copyright-->
<div id="footer-bottom">

    <!-- 960 Container -->
    <div class="container">

        <div class="eight columns">
            <div id="copyright">© Copyright 2012 <span>Bearden</span>. All Rights Reserved.</div>
        </div>

        <div class="eight columns">
            <ul class="social-links">
                <li class="twitter"><a href="#">Twitter</a></li>
                <li class="facebook"><a href="#">Facebook</a></li>
                <li class="digg"><a href="#">Digg</a></li>
                <li class="vimeo"><a href="#">Vimeo</a></li>
                <li class="youtube"><a href="#">YouTube</a></li>
                <li class="skype"><a href="#">Skype</a></li>
            </ul>
        </div>

    </div>
    <!-- End 960 Container -->

</div>
<!--  Footer Copyright End -->

<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>

<!-- Imagebox Build -->
<script src="js/imagebox.build.js"></script>

</body>
</html>