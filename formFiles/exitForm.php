<?

/*  Include the Bearden file */

include_once "BeardenGuidance.php";

/* Create a Bearden Object */

$Bearden = new BeardenGuidance();

/* Check if the user logged in */

if ( !$Bearden->getBeardenUser ()->getIsTakingForm () ) {

    /* They are not logged in, redirect and notify the user */

    $Bearden->getBeardenRedirect ()->redirectUser ( Redirects::TAKE_FORM_FAIL_REDIRECT , "Please fill in your Student ID and the form you need to take!" );

    /*  Check if the user is on the right form */

}

/* Check if an action is being requested */

if ( $Bearden->getBeardenActionHandler ()->getRequestedAction () == NULL ) {


    if ( !strcasecmp ( $Bearden->getBeardenUser ()->getFormType () , $_GET[ 'location' ] ) == 0 ) {

        /* They are not, redirect and notify the user */

        /* Set the location of the user */

        $Bearden->getBeardenUser ()->setLocation ( $Bearden->getBeardenUser ()->getFormType () );

        /*  Redirect and notify the user of what happened */

        $Bearden->getBeardenRedirect ()->redirectUser ( Redirects::RED_REDIRECT , "You were at the wrong form, you're in the right place now!" );

    }
}

/* Check if the requested action is null  */

$Bearden->getBeardenActionHandler ()->getRequestedAction () !== NULL

    /* Execute the requested action */

    ? $Bearden->getBeardenActionHandler ()->executeRequestedAction ( $Bearden->getBeardenActionHandler ()->getRequestedAction () )

    /* There was no action requested, do nothing */

    : FALSE;

?>




<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html class="ie ie6" lang="en"><![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7" lang="en"><![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"><!--<![endif]-->
<head>

    <!-- Basic Page Needs
     ================================================== -->
    <meta charset="utf-8">
    <title> Exit Form - <? echo $Bearden->getBeardenUser ()->getStudentID () ?></title>

    <!-- Mobile Specific
     ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
     ================================================== -->
    <link rel="stylesheet" type="text/css" href="/css/style.css">

    <!-- Java Script
     ================================================== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script src="js/flexslider.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/ender.min.js"></script>
    <script src="js/selectnav.js"></script>
    <script src="js/imagebox.min.js"></script>
    <script src="js/carousel.js"></script>
    <script src="js/twitter.js"></script>
    <script src="js/tooltip.js"></script>
    <script src="js/popover.js"></script>
    <script src="js/effects.js"></script>
    <script src="js/bearden.js"></script>


    <style type="text/css">

        .eight {
            margin: 0 auto;
        }

        #college input {

            margin: 0 auto;
        }

        #college th {
            text-align: center;
        }

        #feedback th {

            text-align: center;

        }

        #feedback td {

            text-align: center;
        }

        .sixteen {
            margin-top: 20px;
        }

        table {
            margin-top: 10px;
        }


    </style>
</head>
<body>

<!-- Header -->
<div id="header">

    <!-- 960 Container Start -->
    <div class="container ie-dropdown-fix">

        <!-- Logo -->
        <div class="four columns">
            <a href="#"><img src="images/logo.png" alt=""/></a>
        </div>

        <!-- Main Navigation Start -->
        <div class="twelve columns">
            <div id="navigation">
                <ul id="nav">

                    <li><a href="/takeForm?action=cancel" id="current">Cancel Form</a></li>

                </ul>
            </div>
        </div>
        <!-- Main Navigation End -->

    </div>
    <!-- 960 Container End -->

</div>
<!-- End Header -->

<div id="page-title">

    <!-- 960 Container Start -->
    <div class="container">
        <div class="sixteen columns">
            <h2>Exit Form - <? echo $Bearden->getBeardenUser ()->getStudentID () ?></h2>
        </div>
    </div>
    <!-- 960 Container End -->

</div>
<!-- 960 Container Start -->
<form method="POST" action="/panel" id="formSubmit">


<div class="container" style="padding-top: 20px;">
<?

/*  Check if the message is empty */

if ( !empty( $_GET[ 'message' ] ) ) {

    /*  Check if the message is GREEN */

    echo isset( $_GET[ 'green' ] )

        /* The message is green, make the HTML markup according  */

        ? '<div class="notification success closeable">'

        /* The message is green, make the HTML markup according  */

        : '<div class="notification error closeable">';

    /*  Now echo the proper message */

    echo '<p><span>' . $_GET[ 'message' ] . '</span></p>';

    /*  End the div for the message box */

    echo '</div>';
}
?>

<div class="sixteen columns">
    <h4 class="headline"> Personal Information</h4>

</div>

<div class="eight columns">

    <table>
        <tr>
            <td>
                <label><b>*</b> First Name:<br/></label>
                <input type="text" size="15" name="firstname" value=""/>
            </td>
        </tr>
    </table>
</div>
<div class="eight columns">
    <table>

        <tr>
            <td>
                <label>Middle Initial:<br/></label>
                <input type="text" name="middlename" size="3" maxlength="1" value=""/>
            </td>
        </tr>
    </table>

</div>

<div class="eight columns">
    <table>

        <tr>
            <td>
                <label><b>*</b> Last Name:<br/></label>
                <input type="text" name="lastname" size="15" maxlength="35" value=""/>
            </td>
        </tr>
    </table>

</div>

<div class="eight columns">
    <table>

        <tr>
            <td>
                <label> <b>*</b>Street Mailing Address:<br/></label>
                <input type="text" name="address" size="35" maxlength="255" value=""/>
            </td>
        </tr>
    </table>

</div>

<div class="eight columns">
    <table>

        <tr>
            <td>
                <label><b>*</b> City:<br/></label>
                <input type="text" name="city" size="20" maxlength="100" value=""/>
            </td>
        </tr>
    </table>

</div>

<div class="eight columns">
    <table>

        <tr>
            <td>
                <label><b>*</b> Zip Code:<br/></label>
                <input type="text" name="zipcode" size="25" maxlength="255" value=""/>
            </td>
        </tr>
    </table>

</div>

<div class="eight columns">
    <table>

        <tr>
            <td>
                <label><b>*</b>Email address:<br/></label>
                <input type="text" name="email" size="25" maxlength="255" value=""/>
            </td>
        </tr>
    </table>
</div>

<div class="eight columns">
    <table>

        <tr>
            <td>
                <label><b>*</b>Student Cell Number<br/></label>
                <input type="text" name="studentcell" size="12" maxlength="255" value=""/>
            </td>
        </tr>
    </table>
</div>

<div class="sixteen columns">
    <h4 class="headline">Military</h4>

</div>
<div class="six columns">
    <table>
        <tr>
            <td>
                <strong>
                    <p style="font-size: small;">If you plan to go into the military, please tell us:</p>
                </strong>

                <label><strong>Have you met with a recruiter yet?</strong></label>
            </td>
        </tr>
        <tr>
            <td>
                <label><input type="radio" name="recruiter" value="Yes"/>Yes</label>
                <label><input type="radio" name="recruiter" value="No"/>No</label>
                <label><input type="radio" name="recruiter" value="Not Applicable" checked="checked"/>Not
                    Applicable</label>


            </td>
        </tr>
    </table>

</div>


<div class="six columns">
    <label><strong>Branch of Service (select one)</strong><br/></label>
    <table>
        <tr>
            <td>
                <select name="branch">
                    <option value="Army">Army</option>
                    <option value="Air Force">Air Force</option>
                    <option value="Navy">Navy</option>
                    <option value="Marine Corps">Marine Corps</option>
                    <option value="Coast Guard">Coast Guard</option>
                    <option value="Police (local Gov't)">Police (local Gov't)</option>
                    <option value="Fire (local Gov't)">Fire (local Gov't)</option>
                    <option value="EMT">EMT</option>
                    <option value="Other">Other</option>
                </select>
            </td>
        </tr>

    </table>
</div>

<div class="four columns">
    <label><strong>Expected starting date:</strong><br/></label>

    <table>
        <tr>
            <td>
                <input type="text" name="startingdate" size="10" maxlength="10" value=""/>

            </td>
        </tr>
    </table>
</div>


<div class="sixteen columns">
    <h4 class="headline"> College and Workforce</h4>

</div>

<div class="eight columns">
    <label><b>*</b><strong> Do you plan to participate in commencement (graduation ceremony)?</strong><br/></label>
    <table>
        <tr>
            <td>
                <label><input type="radio" name="participate" value="Yes"/>Yes</label>
            </td>
        </tr>
        <tr>
            <td>
                <label><input type="radio" name="participate" value="No" checked="checked"/>No</label>
            </td>
        </tr>
    </table>

</div>
<div class="eight columns">
    <label><b>*</b><strong> My plans after graduation are:</strong><br/></label>
    <table>
        <tr>
            <td>

                <select name="graduationplans">
                    <option value="Go into the military">Go into the military</option>
                    <option value="Go to work">Go to work</option>
                    <option value="Attend a technical or vocational school">Attend a technical or vocational
                        school
                    </option>
                    <option value="Attend a 2 year community college">Attend a 2 year community college</option>
                    <option value="Attend a 4 year college or university">Attend a 4 year college or university
                    </option>
                </select>
            </td>
        </tr>

    </table>

</div>

<div class="sixteen columns">
    <label><strong>If planning to enroll in a 2 year community college please choose which
            one</strong></label>
    <table>
        <tr>
            <td>

                <select name="2yearcollege">
                    <option value="Pellissippi State Community College">Pellissippi State Community College</option>
                    <option value="Roane State Community College">Roane State Community College</option>
                    <option value="Walters State Community College">Walters State Community College</option>
                    <option value="Other">Other</option>
                </select>
            </td>
        </tr>
    </table>
</div>


<div class="sixteen columns">
    <label><strong>If you plan to work after graduation and NOT attend college in the fall
            of <? echo date ( "Y" ) + 1 ?>,
            please tell us where.</strong></label>
    <table>
        <tr>
            <td>
                <input type="text" name="workforcegrad" size="25" maxlength="255" value=""/>
            </td>
        </tr>
    </table>
</div>

<div class="sixteen columns">

    <h4 class="headline">College Admission</h4>

</div>

<div class="sixteen columns">
    <table>
        <tr>
            <td>
                <strong>
                    <p>
                        Please list the college to which you were admitted and will be attending. If you have not made a
                        final decision please list your second choice. FINAL TRANSCRIPTS will only be sent to the
                        school/schools listed. (Remember, a final transcript is required to register for college
                        classes.)
                        Don't forget to request a transcript from Pellissippi if you took a Dual Enrollment class in
                        high school!
                    </p>
                </strong>
            </td>
        </tr>
    </table>
</div>


<div class="sixteen columns">

    <table>
        <tr>
            <td>
                <a href="http://www.actstudent.org/scores/scodes.html" class="link" target="_blank">Link to find ACT
                    codes for Schools. </a>
            </td>
        </tr>
        <tr>

    </table>
</div>

<div class="sixteen columns">

    <table id="college" class="standard-table">

        <tr>
            <th></th>
            <th>ACT Code</th>
            <th>School Name</th>
            <th>School City and State</th>
        </tr>

        <tr>
            <td><label>Send my 1st transcript to:</label></td>
            <td><input align="center" type="text" name="college-1-1" size="24" maxlength="255"/></td>
            <td><input align="center" type="text" name="college-1-2" size="24" maxlength="255"/></td>
            <td><input align="center" type="text" name="college-1-3" size="24" maxlength="255"/></td>
        </tr>

        <tr>

            <td>Send my 2nd transcript to:</td>
            <td><input type="text" name="college-2-1" size="24" maxlength="255"/></td>
            <td><input type="text" name="college-2-2" size="24" maxlength="255"/></td>
            <td><input type="text" name="college-2-3" size="24" maxlength="255"/></td>
        </tr>

    </table>
</div>


<div class="sixteen columns">
    <label><b>*</b><strong>If you will play college sports you MUST check here to have a final transcript sent to NCAA
            or NAIA?</strong><br/></label>
    <table>
        <tr>
            <td>
                <label><input type="radio" name="sportrans" value="NCAA"/>NCAA</label>
            </td>
        </tr>
        <tr>
            <td>
                <label><input type="radio" name="sportrans" value="NAIA"/>NAIA</label>
            </td>
        </tr>

    </table>
</div>

<div class="sixteen columns">
    <label><b>*</b><strong>If you do not want any transcripts sent at the end of the school year, please check
            here.</strong><br/></label>
    <table>
        <tr>
            <td>
                <label><input type="checkbox" name="notrans" value="Yes"/>No Transcript</label>
            </td>
        </tr>

    </table>

</div>

<div class="sixteen columns">
    <label><b>*</b><strong>Please list additional colleges where you were offered admission, but have chosen not to
            attend.

            <table id="college" class="standard-table">

                <tr>
                    <th></th>
                    <th>School Name</th>
                    <th>School City and State</th>
                </tr>

                <tr>
                    <td><label>Additional College:</label></td>
                    <td><input align="center" type="text" name="add-1-1" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="add-1-2" size="24" maxlength="255"/></td>
                </tr>

                <tr>
                    <td><label>Additional College:</label></td>
                    <td><input align="center" type="text" name="add-2-1" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="add-2-2" size="24" maxlength="255"/></td>
                </tr>
                <tr>
                    <td><label>Additional College:</label></td>
                    <td><input align="center" type="text" name="add-3-1" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="add-3-2" size="24" maxlength="255"/></td>
                </tr>
                <tr>
                    <td><label>Additional College:</label></td>
                    <td><input align="center" type="text" name="add-4-1" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="add-4-2" size="24" maxlength="255"/></td>
                </tr>
                <tr>
                    <td><label>Additional College:</label></td>
                    <td><input align="center" type="text" name="add-5-1" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="add-5-2" size="24" maxlength="255"/></td>
                </tr>
                <tr>
                    <td><label>Additional College:</label></td>
                    <td><input align="center" type="text" name="add-6-1" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="add-6-2" size="24" maxlength="255"/></td>
                </tr>
            </table>
</div>

<div class="sixteen columns">

    <h4 class="headline">Scholarships</h4>


</div>


<div class="sixteen columns">
    <label><b>*</b><strong>Did you qualify for the Tennessee Lottery HOPE Scholarship? (Unweighted 3.0 GPA OR 21
            ACT)?</strong><br/></label>
    <table>
        <tr>
            <td>
                <label><input type="radio" name="tnhope" value="Yes"/>Yes</label>
            </td>
        </tr>
        <tr>
            <td>
                <label><input type="radio" name="tnhope" value="No"/>No</label>
            </td>
        </tr>
    </table>

</div>
<div class="sixteen columns">
    <label><b>*</b><strong>Did you qualify for additional Tennessee Lottery HOPE Scholarships?</strong><br/></label>
    <table>
        <tr>
            <td>
                <label><input type="checkbox" name="genassemb" value="Yes"/>HOPE General Assembly Merit (GAMS)? (29 ACT
                    & 3.75 GPA)</label>
            </td>
        </tr>
        <tr>
            <td>
                <label><input type="radio" name="aspireward" value="No"/>HOPE Aspire Award (family income $36,000/year
                    or less)</label>
            </td>
        </tr>
    </table>

</div>

<div class="sixteen columns">
    <label><b>*</b><strong>DWere you offered the tnAchieves Scholarship (to attend community
            college)??</strong><br/></label>
    <table>
        <tr>
            <td>
                <label><input type="radio" name="tnachieve" value="Yes"/>Yes</label>
            </td>
        </tr>
        <tr>
            <td>
                <label><input type="radio" name="tnachieve" value="No"/>No</label>
            </td>
        </tr>
    </table>

</div>
<div class="sixteen columns">
    <label><b>*</b><strong>If you were offered scholarships to the college you will be attending (or from other
            sources), please list the information below: (scholarships you plan to ACCEPT should go here)
            <table id="college" class="standard-table">

                <tr>
                    <th></th>
                    <th>Scholarship Name</th>
                    <th>Offered By</th>
                    <th>Renewable how many years?</th>
                    <th>Amount per Yr</th>
                    <th>Accepted / Declined</th>

                </tr>

                <tr>
                    <td><label>Scholarship #1</label></td>
                    <td><input align="center" type="text" name="acc-1-1" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="acc-1-2" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="acc-1-3" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="acc-1-4" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="acc-1-5" size="24" maxlength="255"/></td>

                </tr>

                <tr>
                    <td><label>Scholarship #2</label></td>
                    <td><input align="center" type="text" name="acc-2-1" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="acc-2-2" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="acc-2-3" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="acc-2-4" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="acc-2-5" size="24" maxlength="255"/></td>

                </tr>
                <tr>
                    <td><label>Scholarship #3</label></td>
                    <td><input align="center" type="text" name="acc-3-1" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="acc-3-2" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="acc-3-3" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="acc-3-4" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="acc-3-5" size="24" maxlength="255"/></td>

                </tr>
                <tr>
                    <td><label>Scholarship #4</label></td>
                    <td><input align="center" type="text" name="acc-4-1" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="acc-4-2" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="acc-4-3" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="acc-4-4" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="acc-4-5" size="24" maxlength="255"/></td>

                </tr>
                <tr>
                    <td><label>Scholarship #5</label></td>
                    <td><input align="center" type="text" name="acc-5-1" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="acc-5-2" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="acc-5-3" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="acc-5-4" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="acc-5-5" size="24" maxlength="255"/></td>

                </tr>
            </table>
</div>
<div class="sixteen columns">
    <label><b>*</b><strong>If you were offered scholarships from other colleges or sources (even if they were not
            accepted) please list the information below: (Scholarships you chose NOT TO ACCEPT should go here)
            <table id="college" class="standard-table">

                <tr>
                    <th></th>
                    <th>Scholarship Name</th>
                    <th>Offered By</th>
                    <th>Renewable how many years?</th>
                    <th>Amount per Yr</th>
                    <th>Accepted / Declined</th>

                </tr>

                <tr>
                    <td><label>Scholarship #1</label></td>
                    <td><input align="center" type="text" name="dec-1-1" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="dec-1-2" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="dec-1-3" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="dec-1-4" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="dec-1-5" size="24" maxlength="255"/></td>

                </tr>

                <tr>
                    <td><label>Scholarship #2</label></td>
                    <td><input align="center" type="text" name="dec-2-1" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="dec-2-2" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="dec-2-3" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="dec-2-4" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="dec-2-5" size="24" maxlength="255"/></td>

                </tr>
                <tr>
                    <td><label>Scholarship #3</label></td>
                    <td><input align="center" type="text" name="dec-3-1" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="dec-3-2" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="dec-3-3" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="dec-3-4" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="dec-3-5" size="24" maxlength="255"/></td>

                </tr>
                <tr>
                    <td><label>Scholarship #4</label></td>
                    <td><input align="center" type="text" name="dec-4-1" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="dec-4-2" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="dec-4-3" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="dec-4-4" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="dec-4-5" size="24" maxlength="255"/></td>

                </tr>
                <tr>
                    <td><label>Scholarship #5</label></td>
                    <td><input align="center" type="text" name="dec-5-1" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="dec-5-2" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="dec-5-3" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="dec-5-4" size="24" maxlength="255"/></td>
                    <td><input align="center" type="text" name="dec-5-5" size="24" maxlength="255"/></td>

                </tr>
            </table>
</div>
<div class="sixteen columns">

    <h4 class="headline">Bearden School Counseling Program Input</h4>


</div>


<div class="five columns">
    <label><b>*</b> <strong>Would you like to meet with your counselor to discuss future plans?</strong></label>
    <table>
        <tr>
            <td>
                <label><input type="radio" name="meetwith" value="Yes"/>Yes</label>
            </td>
        </tr>
        <tr>
            <td>
                <label><input type="radio" name="meetwith" value="No" checked="checked"/>No</label>
            </td>
        </tr>
    </table>
</div>

<div class="five columns">
    <label><b>*</b> <strong>Do you feel your counselor treated you with kindness, respect, and dignity?</strong></label>
    <table>
        <tr>
            <td>
                <select name="kindness">
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
        </tr>
    </table>


</div>

<div class="sixteen columns">
    <label><b>*</b> <strong>Please provide your feedback to help us shape and improve BHS Guidance
            services:</strong></label>

    <table class="standard-table" id="feedback">
        <tr>
            <th>&nbsp;</th>
            <th>Never</th>
            <th>Sometimes</th>
            <th>Mostly</th>
            <th>Always</th>
            <th>Not Applicable</th>
        </tr>
        <tr>
            <td>I felt welcome in the counseling office</td>
            <td><input type="radio" name="question-1-1" value="Never"/></td>
            <td><input type="radio" name="question-1-1" value="Sometimes"/></td>
            <td><input type="radio" name="question-1-1" value="Mostly"/></td>
            <td><input type="radio" name="question-1-1" value="Always"/></td>
            <td><input type="radio" name="question-1-1" value="Not Applicable" checked="checked"/></td>
        </tr>
        <tr>
            <td>I felt comfortable talking with my counselor</td>
            <td><input type="radio" name="question-2-1" value="Never"/></td>
            <td><input type="radio" name="question-2-1" value="Sometimes"/></td>
            <td><input type="radio" name="question-2-1" value="Mostly"/></td>
            <td><input type="radio" name="question-2-1" value="Always"/></td>
            <td><input type="radio" name="question-2-1" value="Not Applicable" checked="checked"/></td>
        </tr>
        <tr>
            <td>How available was your counselor?</td>
            <td><input type="radio" name="question-3-1" value="Never"/></td>
            <td><input type="radio" name="question-3-1" value="Sometimes"/></td>
            <td><input type="radio" name="question-3-1" value="Mostly"/></td>
            <td><input type="radio" name="question-3-1" value="Always"/></td>
            <td><input type="radio" name="question-3-1" value="Not Applicable" checked="checked"/></td>
        </tr>
    </table>

</div>


<div class="sixteen columns">
    <label><b>*</b>
        <strong>
            Please rank the effectiveness and level of caring of your counselor.&nbsp; (note: a 1
            ranking = not applicable; a&nbsp;10 ranking is the highest)
        </strong>
    </label>

    <table class="standard-table" id="feedback">
        <tr>
            <th>&nbsp;</th>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>
            <th>10</th>
        </tr>
        <tr>
            <td>Counselor assistance for academic issues:</td>
            <td><input type="radio" name="academic_issues" value="1"/></td>
            <td><input type="radio" name="academic_issues" value="2"/></td>
            <td><input type="radio" name="academic_issues" value="3"/></td>
            <td><input type="radio" name="academic_issues" value="4"/></td>
            <td><input type="radio" name="academic_issues" value="5"/></td>
            <td><input type="radio" name="academic_issues" value="6"/></td>
            <td><input type="radio" name="academic_issues" value="7"/></td>
            <td><input type="radio" name="academic_issues" value="8"/></td>
            <td><input type="radio" name="academic_issues" value="9"/></td>
            <td><input type="radio" name="academic_issues" value="10"/></td>
        </tr>
        <tr>
            <td>Counselor assistance for college/career planning:</td>
            <td><input type="radio" name="career_planning" value="1"/></td>
            <td><input type="radio" name="career_planning" value="2"/></td>
            <td><input type="radio" name="career_planning" value="3"/></td>
            <td><input type="radio" name="career_planning" value="4"/></td>
            <td><input type="radio" name="career_planning" value="5"/></td>
            <td><input type="radio" name="career_planning" value="6"/></td>
            <td><input type="radio" name="career_planning" value="7"/></td>
            <td><input type="radio" name="career_planning" value="8"/></td>
            <td><input type="radio" name="career_planning" value="9"/></td>
            <td><input type="radio" name="career_planning" value="10"/></td>
        </tr>

        <tr>
            <td>Counselor assistance for personal/social needs:</td>
            <td><input type="radio" name="social_needs" value="1"/></td>
            <td><input type="radio" name="social_needs" value="2"/></td>
            <td><input type="radio" name="social_needs" value="3"/></td>
            <td><input type="radio" name="social_needs" value="4"/></td>
            <td><input type="radio" name="social_needs" value="5"/></td>
            <td><input type="radio" name="social_needs" value="6"/></td>
            <td><input type="radio" name="social_needs" value="7"/></td>
            <td><input type="radio" name="social_needs" value="8"/></td>
            <td><input type="radio" name="social_needs" value="9"/></td>
            <td><input type="radio" name="social_needs" value="10"/></td>
        </tr>
    </table>


</div>


<div class="sixteen columns">

    <table class="standard-table">
        <tr>
            <th>Indicate which events you, your parent, or guardian have attended or plan to attend [Check all that
                apply]
            </th>

        </tr>
        <tr>
            <td>
                <label>
                    <input type="checkbox" name="event1" value="College Game On during Dawg Day"/>
                    College Game On during Dawg Day
                </label>
            </td>
        </tr>
        <tr>
            <td><label><input type="checkbox" name="event2" value="College Cafe during early release"/>College Cafe
                    during early release</label></td>
        </tr>
        <tr>
            <td><label><input type="checkbox" name="event3"
                              value="Open House grade-level meetings (in auditorium or library prior to open house)"/>Open
                    House grade-level meetings (in auditorium or library prior to open house)</label></td>
        </tr>
        <tr>
            <td><label><input type="checkbox" name="event4" value="Financial Aid Workshop (FAFSA Session in December)"/>Financial
                    Aid Workshop (FAFSA Session in December)</label></td>
        </tr>
        <tr>
            <td><label><input type="checkbox" name="event5" value="College Workshops (Fall 2012)"/>College Workshops
                    (Fall 2012)</label></td>
        </tr>
        <tr>
            <td><label><input type="checkbox" name="event6" value="BHS/FHS/HVA College Fair (October each year)"/>BHS/FHS/HVA
                    College Fair (October each year)</label></td>
        </tr>
        <tr>
            <td><label><input type="checkbox" name="event7" value="Frequented the BHS Guidance website"/>Frequented the
                    BHS Guidance website</label></td>
        </tr>
        <tr>
            <td><label><input type="checkbox" name="event8"
                              value="Met with counselor or staff in the College & Career Center"/>Met with counselor or
                    staff in the College & Career Center</label></td>
        </tr>
        <tr>
            <td><label><input type="checkbox" name="event9"
                              value="Met with school counselor to review graduation requirements"/>Met with school
                    counselor to review graduation requirements</label></td>
        </tr>
        <tr>
            <td><label><input type="checkbox" name="event10" value="None of the above"/>None of the above</label></td>
        </tr>
    </table>


</div>
<div class="sixteen columns">


    <table class="standard-table">
        <tr>
            <th> Indicate areas you would have liked more assistance [Check all that apply]</th>
        </tr>

        <tr>
            <td>
                <label>
                    <input type="checkbox" name="more_assistance1" value="Yes"/>Personal - Grief
                </label>
            </td>
        </tr>
        <tr>

            <td>
                <label>
                    <input type="checkbox" name="more_assistance2" value="Yes"/>Personal -
                    Conflict
                </label>
            </td>
        </tr>
        <tr>
            <td><label><input type="checkbox" name="more_assistance3" value="Yes"/>Personal -
                    Personal Issues</label></td>
        <tr>

            <td><label><input type="checkbox" name="more_assistance4" value="Yes"/>Academic
                    - Test Taking/Study Skills</label></td>
        </tr>
        <tr>
            <td><label><input type="checkbox" name="more_assistance5" value="Yes"/>Academic -
                    Course Advising</label></td>
        </tr>
        <tr>
            <td><label><input type="checkbox" name="more_assistance6" value="Yes"/>Academic -
                    Time Management</label></td>
        </tr>
        <tr>
            <td><label><input type="checkbox" name="more_assistance7" value="Yes"/>Career Planning</label>
            </td>
        </tr>
        <tr>
            <td><label><input type="checkbox" name="more_assistance8" value="Yes"/>College Planning</label>
            </td>
        </tr>
        <tr>
            <td><label><input type="checkbox" name="more_assistance9" value="Yes"/>Career
                    - Financial Aid/Scholarships</label></td>
        </tr>
        <tr>
            <td><label><input type="checkbox" name="more_assistance10" value="Yes"/>Other
                    (explain)</label><input
                    type="text" name="explain_box" size="25" maxlength="255" value=""/></td>
        </tr>
    </table>


</div>

<div class="sixteen columns">
    <label><strong>Looking back, what advice would you give a freshman or sophomore student?</strong></label>
    <input type="text" name="advice" size="60" value=""/>
    <a class="button medium green" id="submit" onclick="checkFormFields()">Submit Form</a>
</div>

</div>

<input type="hidden" name="studentID" value="<? echo $Bearden->getBeardenUser ()->getStudentID (); ?>"/>
<input type="hidden" name="action" value="exitFormSubmit"/>
<input type="hidden" name="location" value="<? echo $Bearden->getBeardenUser ()->getLocation (); ?>"/>
</form>


<div id="footer">

    <!-- 960 Container -->
    <div class="container">

        <!-- About -->


        <!-- Twitter -->


    </div>
    <!-- 960 Container End -->

</div>
<!-- Footer End -->

<!--  Footer Copyright-->
<div id="footer-bottom">

    <!-- 960 Container -->
    <div class="container">

        <div class="eight columns">
            <div id="copyright">© Copyright 2012 <span>Voltrex</span>. All Rights Reserved.</div>
        </div>

        <div class="eight columns">
            <ul class="social-links">
                <li class="twitter"><a href="#">Twitter</a></li>
                <li class="facebook"><a href="#">Facebook</a></li>
                <li class="digg"><a href="#">Digg</a></li>
                <li class="vimeo"><a href="#">Vimeo</a></li>
                <li class="youtube"><a href="#">YouTube</a></li>
                <li class="skype"><a href="#">Skype</a></li>
            </ul>
        </div>

    </div>
    <!-- End 960 Container -->

</div>
<!--  Footer Copyright End -->

<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>

<!-- Imagebox Build -->
<script src="js/imagebox.build.js"></script>

</body>
</html>