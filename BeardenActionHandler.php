<?php
/**
 * Created by JetBrains PhpStorm.
 * User: elialanharris
 * Date: 7/24/13
 * Time: 2:02 PM
 * To change this template use File | Settings | File Templates.
 */


/**
 * Class BeardenActionHandler
 */

include_once "Constants.php";

/**
 * Class BeardenActionHandler
 */
class BeardenActionHandler extends BeardenGuidance
{

    /**
     * @var BeardenActions
     */

    private $BeardenActions;

    /**
     * @return The action requested in it's constant, numerical form
     */

    public function getRequestedAction ()
    {
        /* Check if the post value 'action' or the get value 'action' is not null */

        if ( empty( $_POST[ 'action' ] ) == FALSE || empty( $_GET[ 'action' ] ) == FALSE ) {

            /* Check if the post value 'action' is  null */

            empty( $_POST[ 'action' ] )

                /* It's null, so will will return the get value of 'action' */

                ? $requestedAction = $_GET[ 'action' ]

                /* It's not null, so will will return the post value of 'action' */

                : $requestedAction = $_POST[ 'action' ];

            /* Return the requested action */

            return $requestedAction;


        } else {

            /* Return NULL, we should never reach this point */

            RETURN NULL;
        }

    }

    /**
     * @param $ACTION
     * Executes the requested action POST/GET
     */
    public function executeRequestedAction ( $ACTION )
    {

        /* Switch through the requested action POST/GET */

        switch ( $ACTION ) {

            /* The user is requesting to login */

            case 'login':

                /*  Execute the login action */

                $this->getBeardenActions ()->loginAction ();

                break;

            /* The user is requesting to register an account */

            case 'register':

                /* Execute the register action */

                $this->getBeardenActions ()->registerAction ();

                break;

            /* The user is requesting to logout */

            case 'logout':

                /* Execute the logout action */

                $this->getBeardenActions ()->logoutAction ();

                break;


            /* A student want to take the form */

            case 'takeForm':

                /* Execute the take form action*/

                $this->getBeardenActions ()->takeFormAction ();


                break;

            /* A student is submitting their entryForm*/

            case 'entryFormSubmit':

                /* Execute the submit form action */


                $this->getBeardenActions ()->entryFormSubmitAction ();

                break;


            /* A student is submitting their entryForm*/

            case 'exitFormSubmit':

                /* Execute the submit form action */


                $this->getBeardenActions ()->exitFormSubmitAction ();

                break;


            /* A student is cancelling their form */

            case 'cancel':

                /* Execute the cancel action */

                $this->getBeardenActions ()->cancelFormAction ();

                break;

            /* Updating an entry form */

            case 'updateEntryForm':

                /* Execute the update entry form action */

                $this->getBeardenActions ()->updateEntryFormAction ();

                break;


            /* Updating an exit form */

            case 'updateExitForm':

                /* Execute the update entry form action */

                $this->getBeardenActions ()->updateExitFormAction ();

                break;

            /* Deleting a form from the database */

            case 'deleteForm':

                /* Execute action for deleting the form */

                $this->getBeardenActions ()->deleteForm ();

                break;


            /* The default case, we should never reach here  */

            default:

                /* Redirect the user to the location they are at and notify them  */

                $this->getBeardenRedirect ()->redirectUser ( Redirects::RED_REDIRECT , "Action not recognized!" );

                break;
        }

    }

    /**
     * @return \BeardenActions
     */

    public function getBeardenActions ()
    {
        /* Check if Bearden Action is NULL */

        $this->BeardenActions == NULL

            /* If it is NULL, we need to include the object file */

            ? include_once "BeardenActions.php"

            /* It's already been set, now we just return it */

            : FALSE;


        /* Check if Bearden Action is NULL */

        $this->BeardenActions == NULL

            /* If it is NULL, we need to create a new Bearden Action object */

            ? $this->BeardenActions = new BeardenActions()

            /* It's already been set, now we just return it */

            : FALSE;

        /* Return the object */

        return $this->BeardenActions;
    }


}