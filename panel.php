﻿<?php

/* Created by Eli Harris on Monday, March 22, 201
  File:Panel.php
  Panel script for Bearden
  Note: The methods in this file are executed directly from config.php: Consult there to edit methods
 */

/* Require the Bearden file */


switch ( $_REQUEST[ 'location' ] ) {

    /* The form results page is being requested */

    case 'forumResults':

        /*  Include the formResults file */

        include_once 'websiteFiles/formResults.php';

        break;

    /* The user wants to take the entry form */

    case 'entryForm':

        /*  Include the entryForm file */

        include_once 'formFiles/entryForm.php';


        break;

    /* The user wants to take the exit form */

    case 'exitForm':

        /*  Include the exitForm file */

        include_once 'formFiles/exitForm.php';


        break;

    /* A counselor is trying to view the results */

    case 'viewEntryFormResults':

        /*  Include the exitForm file */

        include_once 'websiteFiles/viewEntryFormResults.php';


        break;

    /* A counselor is trying to view the results */

    case 'viewExitFormResults':

        /*  Include the exitForm file */

        include_once 'websiteFiles/viewExitFormResults.php';


        break;

    /* The default page is being requested */


    default:

        include_once 'websiteFiles/formResults.php';

        break;

}




