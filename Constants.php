<?php
/**
 * Created by JetBrains PhpStorm.
 * User: elialanharris
 * Date: 7/12/13
 * Time: 11:24 AM
 * To change this template use File | Settings | File Templates.
 */
interface Redirects
{
    /**
     * Constant that defines the location of the redirect when the user logs-in
     */

    const LOGIN_SUCCESS_REDIRECT = 0;
    /**
     * Constant that defines the location of the redirect when the user fails to login
     */

    const LOGIN_FAIL_REDIRECT = 1;
    /**
     * Constant that defines the location of the redirect when the user tried to access a logged in page only
     */

    const PLEASE_LOGIN_REDIRECT = 2;
    /**
     * Constant that defines the location of the redirect when the user logs out
     */

    const LOGOUT_REDIRECT = 3;
    /**
     * Constant that defines the location of the redirect when registration fails
     */

    const REGISTER_SUCCESS_REDIRECT = 4;
    /**
     * Constant that defines the location of the redirect when registration succeeds
     */

    const REGISTER_FAILED_REDIRECT = 5;
    /**
     * Constant that defines the take form action failed
     */
    const TAKE_FORM_FAIL_REDIRECT = 6;
    /**
     * Constant that defines the take form action failed
     */
    const TAKE_FORM_SUCCESS_REDIRECT = 7;
    /**
     * Constant that defines a red box will be displayed when redirected
     */
    const RED_REDIRECT = 8;
    /**
     * Constant that defines a green box will be displayed when redirected
     */
    const GREEN_REDIRECT = 9;
    /**
     * Constant that defines a red box will be displayed when redirected
     */
    const FORM_RESULTS_RED_REDIRECT = 10;
    /**
     * Constant that defines a green box will be displayed when redirected
     */
    const FORM_RESULTS_GREEN_REDIRECT = 11;


}

/**
 * Class Query
 */
interface Query
{

    /**
     * Defines the username of the MySQL database
     */
    const MYSQL_USER = "root";
    /**
     * Defines the password of the MySQL database
     */
    const MYSQL_PASS = "papa1212";
    /**
     * Defines the server we connect to for the database
     */
    const MYSQL_SERVER = "localhost";
    /**
     * Defines the database we select
     */
    const MYSQL_DATABASE = "BeardenGuidance";
    /**
     * Constant that defines the query string for executeQuery method for logging in a user
     */

    const LOGIN_QUERY = "SELECT * FROM administrators WHERE username = '%s' AND password =  '%s'";
    /**
     * Constant that defines the query string for checking if the student ID already exists
     */
    const CHECK_STUDENT_ID = "SELECT * FROM `%s` WHERE studentID = '%s'";
    /**
     * Constant that defines the query string for getting all the entry forms
     */
    const GET_ENTRY_FORMS_QUERY = "SELECT * FROM entryForm ORDER BY lastname ASC";
    /**
     * Constant that defines the query string for getting all the entry forms
     */
    const GET_EXIT_FORMS_QUERY = "SELECT * FROM exitForm ORDER BY lastname ASC";
    /**
     * Constant that defines the query string for getting a single entry form
     */
    const GET_ENTRY_FORM = "SELECT * FROM entryForm WHERE `order`  = '%s'";
    /**
     * Constant that defines the query string for getting a single exit form
     */
    const GET_EXIT_FORM = "SELECT * FROM exitForm WHERE `order`  = '%s'";
    /**
     * Constant that defines the query string for deleting a form
     */
    const DELETE_FORM = "DELETE FROM `%s` WHERE `order` = '%s'";


}

/**
 * Class CheckData
 */
interface CheckData
{
    /**
     * Constant that defines that login data is being checked
     */

    const LOGIN_DATA = 0;
    /**
     * Constant that defines that register data is being checked
     */
    const REGISTER_DATA = 1;
    /**
     * Constant that defines that the take form data is being checked
     */
    const TAKE_FORM_DATA = 2;

}

