<?php
/**
 * Created by JetBrains PhpStorm.
 * User: elialanharris
 * Date: 7/24/13
 * Time: 1:17 PM
 * To change this template use File | Settings | File Templates.
 */


/**
 * Class BeardenCheckData
 */
class BeardenCheckData extends BeardenGuidance implements CheckData
{


    /**
     * @param $data
     * @param $dataType
     * @return array|null
     * Checks the specified data
     */

    public function checkData ( $data , $dataType )
    {

        /* Find a constant value that matches data type */

        switch ( $dataType ) {

            /* Case for checking the login data */

            case $this::LOGIN_DATA:


                /* The regex that will validate the username */

                $usernameRegEx = "/^[a-z0-9_-]{3,16}$/";

                /* The regex that will validate the password */

                $passwordRegEx = "/^[a-z0-9_-]{6,18}$/";

                /* Checking if the data is null, would signify the user did not supply and credentials */

                $data == null

                    /* The data is null, redirect the user and notify */

                    ? $this->getBeardenRedirect ()->redirectUser ( Redirects::LOGIN_FAIL_REDIRECT , "Please enter both a username and password!" )

                    /* The data was not null, continue checking the data */

                    : FALSE;

                /* Check to see if the credentials are empty */

                empty ( $data[ 'username' ] ) || empty( $data[ 'password' ] ) == TRUE

                    /* Some or all credentials are empty, redirect and notify the user */

                    ? $this->getBeardenRedirect ()->redirectUser ( Redirects::LOGIN_FAIL_REDIRECT , "Please enter both a username and password" )

                    /* The user entered both a username and password, we can continue */

                    : FALSE;

                /* Check if the username is valid by using the regex string */

                preg_match ( $usernameRegEx , $data[ 'username' ] ) == FALSE

                    /* The username is invalid, redirect and notify the user */

                    ? $this->getBeardenRedirect ()->redirectUser ( Redirects::LOGIN_FAIL_REDIRECT , "Invalid username entered!" )

                    /* The username is valid, we can continue checking the data */

                    : FALSE;

                /* Check if the password is valid by using the regex string */

                preg_match ( $passwordRegEx , $data[ 'password' ] ) == FALSE

                    /* The password is invalid, redirect and notify the user */

                    ? $this->getBeardenRedirect ()->redirectUser ( Redirects::LOGIN_FAIL_REDIRECT , "Invalid password entered!" )

                    /* The password is valid, we can continue checking the data */

                    : FALSE;

                /* Clean the data with the mysql_real_escape string, prepare it to be passed in a query */

                $cleanedParameters = array_map ( 'mysql_real_escape_string' , $data );

                /* Clean the data with the filter_var, remove any html that could be in the data */

                $cleanedParameters = array_map ( 'filter_var' , $cleanedParameters );

                /* Return the cleaned parameters, everything checked out */

                return $cleanedParameters;

                break;

            /* Case for checking the register data */

            case $this::REGISTER_DATA:


                /* Require the RECAPTCHA library for verifying the user is not a bot */

                require_once ( 'recaptchalib.php' );

                /* The private key used for the RECAPTCHA library */

                $privateKey = "6LfNvuASAAAAAEkDl4Zr9j0AwhYiG7WK-izauHHh";

                /* Get the response of the captcha */

                $resp = recaptcha_check_answer ( $privateKey , $_SERVER[ "REMOTE_ADDR" ] , $_POST[ "recaptcha_challenge_field" ] , $_POST[ "recaptcha_response_field" ] );

                /* The regex that will validate the email */

                $emailRegEx = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/";

                /* The regex that will validate the username */

                $usernameRegEx = "/^[a-zA-Z0-9_-]{3,16}$/";

                /* The regex that will validate the password */

                $passwordRegEx = "/^[a-zA-Z0-9_-]{6,18}$/";

                /* Convert the register data to JSON */

                $dataToJSON = json_decode ( json_encode ( $data ) );

                /* Check if the RECAPTCHA response is valid */

                $resp->is_valid == FALSE

                    /* An invalid RECAPTCHA was entered, redirect and notify the user */

                    ? $this->getBeardenRedirect ()->redirectUser ( Redirects::REGISTER_FAILED_REDIRECT , "The CAPTCHA was entered was invalid!" )

                    /* A valid RECAPTCHA was entered, we can continue */

                    : FALSE;

                /* Checking if the data is null, would signify the user did not supply and data */

                $data == null

                    /* The data is null, redirect the user and notify */

                    ? $this->getBeardenRedirect ()->redirectUser ( Redirects::REGISTER_FAILED_REDIRECT , "Please enter both a username and password!" )

                    /* The data was not null, we can continue checking the data  */

                    : FALSE;

                /* Check to make sure the user filled in all the required fields */

                empty( $dataToJSON->username ) || empty( $dataToJSON->password ) || empty( $dataToJSON->confirm ) || empty( $dataToJSON->email )

                    /* The user did not fill in all the fields, redirect and notify the user */

                    ? $this->getBeardenRedirect ()->redirectUser ( Redirects::REGISTER_FAILED_REDIRECT , "Please fill in all the required fields!" )

                    /* They filled in all the fields, continue checking the data */

                    : FALSE;

                /* Check to see if the email is valid by using the email regex string */

                preg_match ( $emailRegEx , $dataToJSON->email ) == FALSE

                    /* The email is invalid, redirect and notify the user */

                    ? $this->getBeardenRedirect ()->redirectUser ( Redirects::REGISTER_FAILED_REDIRECT , "Invalid email address! Please enter a valid one! Keep in mind, you will have to verify the email!" )

                    /* The email is valid, continue checking the data */

                    : FALSE;

                /* Check to see if the username is valid by using the username regex string */

                preg_match ( $usernameRegEx , $dataToJSON->username ) == FALSE

                    /* The username is invalid, redirect and notify the user */

                    ? $this->getBeardenRedirect ()->redirectUser ( Redirects::REGISTER_FAILED_REDIRECT , "You entered an invalid username!" )

                    /* The username is valid, continue checking the data */

                    : FALSE;

                /* Check to see if the password and confirm password is valid by using the password regex string */

                preg_match ( $passwordRegEx , $dataToJSON->password ) == FALSE || preg_match ( $passwordRegEx , $dataToJSON->confirm ) == FALSE

                    /* The password or confirm password is invalid, redirect and notify the user */

                    ? $this->getBeardenRedirect ()->redirectUser ( Redirects::REGISTER_FAILED_REDIRECT , "You entered an invalid password!" )

                    /* The password is valid, continue checking the data */

                    : FALSE;

                /* Check if the password and confirm password match */

                strcmp ( $dataToJSON->password , $dataToJSON->confirm ) !== 0

                    /* The did not match, redirect and notify the user */

                    ? $this->getBeardenRedirect ()->redirectUser ( Redirects::REGISTER_FAILED_REDIRECT , "Your password did not match!" )

                    /* The passwords matched, we can continue checking the data */

                    : FALSE;

                /* Check if the user supplied a referral username */

                empty( $dataToJSON->refer ) == FALSE

                    /* The referral is not empty, we need to now verify it with the username regex string */

                    ? preg_match ( $usernameRegEx , $dataToJSON->refer ) == FALSE

                    /* It's an invalid referral, redirect and notify the user */

                    ? $this->getBeardenRedirect ()->redirectUser ( Redirects::REGISTER_FAILED_REDIRECT , "You entered an invalid referral. Please leave blank if you do not have one!" )

                    /* It was a valid referral, we can continue to cleaning the data */

                    : FALSE

                    /* The user did not supply a referral, we do nothing */

                    : FALSE;


                /* Clean the data with the mysql_real_escape string, prepare it to be passed in a query */

                $cleanedParameters = array_map ( 'mysql_real_escape_string' , $data );

                /* Clean the data with the filter_var, remove any html that could be in the data */

                $cleanedParameters = array_map ( 'filter_var' , $cleanedParameters );

                /* Return the cleaned parameters, everything checked out */

                return $cleanedParameters;

                break;

            /*  The data of the take form action is being checked */

            case $this::TAKE_FORM_DATA:

                /* Convert the register data to JSON */

                $dataToJSON = json_decode ( json_encode ( $data ) );

                /*  Regex for checking the student ID*/

                $studentIDRegex = "/^\d{5,7}$/";

                /* Check to see if the username is valid by using the username regex string */

                preg_match ( $studentIDRegex , $dataToJSON->studentID ) == FALSE

                    /* The student ID invalid, redirect and notify the user */

                    ? $this->getBeardenRedirect ()->redirectUser ( Redirects::TAKE_FORM_FAIL_REDIRECT , "You entered an invalid student ID!" )

                    /* The student ID is valid, continue checking the data */

                    : FALSE;

                /*  Check if the user tampered with the HTML*/

                !( strcmp ( $dataToJSON->formType , "exitForm" ) == 0 || strcmp ( $dataToJSON->formType , "entryForm" ) == 0 )

                    /*  They tampered with the HTML, redirect and notify the user*/

                    ? $this->getBeardenRedirect ()->redirectUser ( Redirects::TAKE_FORM_FAIL_REDIRECT , "You entered an invalid form type!" )

                    /*  Everything checked out */

                    : FALSE;

                /* Clean the data with the mysql_real_escape string, prepare it to be passed in a query */

                $cleanedParameters = array_map ( 'mysql_real_escape_string' , $data );

                /* Clean the data with the filter_var, remove any html that could be in the data */

                $cleanedParameters = array_map ( 'filter_var' , $cleanedParameters );

                /* Return the cleaned parameters, everything checked out */

                return $cleanedParameters;


                break;

        }

        /* Return NULL, we should never get here */

        return NULL;
    }

}