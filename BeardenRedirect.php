<?php
/**
 * Created by JetBrains PhpStorm.
 * User: elialanharris
 * Date: 7/24/13
 * Time: 1:20 PM
 * To change this template use File | Settings | File Templates.
 */

/* Include the constants file */

include_once "Constants.php";

class BeardenRedirect extends BeardenGuidance implements Redirects
{


    /**
     * @param $LOCATION
     * @param $message
     * @return mixed
     */

    public function redirectUser ( $LOCATION , $message )
    {
        /* Find the constant that matches $LOCATION */

        switch ( $LOCATION ) {

            /* Case for when the user successfully logs in */

            case $this::LOGIN_SUCCESS_REDIRECT:

                /* Redirects the user back to the login page with an optional message attached */

                header ( "Location: /panel" );

                break;

            /* Case for when the user does not successfully log in */

            case $this::LOGIN_FAIL_REDIRECT:

                /* Redirects the user back to the login page with an optional message attached */

                header ( "Location: /index?message=" . $message );

                break;

            /* Case for when the user needs to log in to be able to access a page */

            case $this::PLEASE_LOGIN_REDIRECT:

                /* Redirects the user back to the login page with an optional message attached */

                header ( "Location: /index?message=" . $message );

                break;

            /* Case for when the user logs out */

            case $this::LOGOUT_REDIRECT:

                /* Redirects the user back to the login page with an optional message attached */

                header ( 'Location:  /index?message=' . $message . '&green' );

                break;

            /* Case for when the registering of a user fails */

            case $this::REGISTER_FAILED_REDIRECT:

                /* Redirects the user back to the register page with an optional message attached */

                header ( 'Location: /register?message=' . $message . '' );

                break;

            /* Case for when taking the form fails, most likely entered invalid student ID */

            case $this::TAKE_FORM_FAIL_REDIRECT:

                /* Redirects the user back to the register page with an optional message attached */

                header ( 'Location: /takeForm?message=' . $message . '' );

                break;

            /* Case for redirecting the user back to where they were and display a red text box */

            case $this::RED_REDIRECT:

                /* Redirects the user back to the location they were just at  with an optional message attached */

                header ( 'Location: /panel?message=' . $message . '&location=' . $this->getBeardenUser ()->getLocation () . '' );

                break;

            /* Case for redirecting the user back to where they were and display a green text box */


            case $this::GREEN_REDIRECT:

                /* Redirects the user back to the location they were just at  with an optional message attached */

                header ( 'Location: /panel?message=' . $message . '&location=' . $this->getBeardenUser ()->getLocation () . '&green' );

                break;

            /* The user successfully submitted their form */

            case $this::TAKE_FORM_SUCCESS_REDIRECT:

                /* Destroy the session */

                session_start ();

                /* Create a new session */

                session_destroy ();

                header ( 'Location: /takeForm?message=' . $message . '&green' );

                break;

            /*  Case for when we are redirecting to results page with a green message */

            case $this::FORM_RESULTS_GREEN_REDIRECT:

                /* Redirects the user back to the location they were just at  with an optional message attached */

                header ( 'Location: /panel?message=' . $message . '&location=formResults&green' );

                break;

            /*  Case for when we are redirecting to results page with a red message */


            case $this::FORM_RESULTS_RED_REDIRECT:

                /* Redirects the user back to the location they were just at  with an optional message attached */

                header ( 'Location: /panel?message=' . $message . '&location=formResults' );

                break;

            /* Case for when there is not matching constant, we should never reach this point  */

            default:

                break;

        }
        exit;
    }


}