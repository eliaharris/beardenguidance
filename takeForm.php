<?php

/* Include the Bearden file */

include_once "BeardenGuidance.php";

/* Create a new Bearden Object */

$Bearden = new BeardenGuidance();

/* Check if the requested action is null  */

$Bearden->getBeardenActionHandler ()->getRequestedAction () !== NULL

    /* Execute the requested action */

    ? $Bearden->getBeardenActionHandler ()->executeRequestedAction ( $Bearden->getBeardenActionHandler ()->getRequestedAction () )

    /* There was no action requested, do nothing */

    : FALSE;


?>

<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html class="ie ie6" lang="en"><![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7" lang="en"><![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"><!--<![endif]-->
<head>

    <!-- Basic Page Needs
     ================================================== -->
    <meta charset="utf-8">
    <title>Take Form</title>

    <!-- Mobile Specific
     ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
     ================================================== -->
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <!-- Java Script
     ================================================== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script src="js/flexslider.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/ender.min.js"></script>
    <script src="js/selectnav.js"></script>
    <script src="js/imagebox.min.js"></script>
    <script src="js/carousel.js"></script>
    <script src="js/twitter.js"></script>
    <script src="js/tooltip.js"></script>
    <script src="js/popover.js"></script>
    <script src="js/effects.js"></script>

</head>
<body>

<!-- Header -->
<div id="header">

    <!-- 960 Container Start -->
    <div class="container ie-dropdown-fix">

        <!-- Logo -->
        <div class="four columns">

        </div>

        <!-- Main Navigation Start -->
        <div class="twelve columns">
            <div id="navigation">
                <ul id="nav">
                    <li><a href="/index">Counselor Login</a></li>

                    <li><a id="current" href="/takeForm">Take Form</a></li>


                </ul>
            </div>
        </div>
        <!-- Main Navigation End -->

    </div>
    <!-- 960 Container End -->

</div>
<!-- End Header -->
<div id="page-title">

    <!-- 960 Container Start -->
    <div class="container">
        <div class="sixteen columns">
            <h2>Take Form</h2>
        </div>
    </div>
    <!-- 960 Container End -->

</div>

<!-- 960 Container Start -->
</br>
<div class="container">
    <div class="sixteen columns">
        <div class="large-notice" style="width: 50%; margin: 0 auto;">
            <img style="margin: 0 auto;" src="images/logo.png"/>
            <br/>

            <form action="/takeForm" method="post">

                <input type="hidden" name="action" value="takeForm"/>

                <table cellspacing="0" cellpadding="0" style="margin: 0 auto;">

                    <tr>
                        <td>
                            <label>Student ID:</label>
                            <input type="text" name="studentID" size="40">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Which form do you need to take?</label>
                            <select name="formType">
                                <option value="entryForm">Entry Form</option>
                                <option value="exitForm">Exit Form</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <input type="submit" value="Take Form" class="button medium green">
                        </td>
                    </tr>
                </table>
            </form>
            <?

            /*  Check if the message is empty */

            if ( !empty( $_GET[ 'message' ] ) ) {

                /*  Check if the message is GREEN */

                echo isset( $_GET[ 'green' ] )

                    /* The message is green, make the HTML markup according  */

                    ? '<div class="notification success closeable" style="text-align:center;">'

                    /* The message is green, make the HTML markup according  */

                    : '<div class="notification error closeable" style="text-align:center;">';

                /*  Now echo the proper message */

                echo '<p><span>' . htmlspecialchars ( $_GET[ 'message' ] ) . '</span></p>';

                /*  End the div for the message box */

                echo '</div>';
            }
            ?>

        </div>


    </div>
</div>
<div id="footer">

    <!-- 960 Container -->
    <div class="container">

        <!-- About -->


        <!-- Twitter -->


    </div>
    <!-- 960 Container End -->

</div>
<!-- Footer End -->

<!--  Footer Copyright-->
<div id="footer-bottom">

    <!-- 960 Container -->
    <div class="container">

        <div class="eight columns">
            <div id="copyright">© Copyright 2012 <span>Bearden</span>. All Rights Reserved.</div>
        </div>


    </div>
    <!-- End 960 Container -->

</div>
<!--  Footer Copyright End -->

<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>

<!-- Imagebox Build -->
<script src="js/imagebox.build.js"></script>

</body>
</html>