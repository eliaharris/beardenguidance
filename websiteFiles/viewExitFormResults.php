<?

/*  Include the Bearden file */

include_once "BeardenGuidance.php";

/* Create a Bearden Object */

$Bearden = new BeardenGuidance();

/* Check if the user logged in */

if ( !$Bearden->getBeardenUser ()->getIsLoggedIn () ) {

    /* They are not logged in, redirect and notify the user */

    $Bearden->getBeardenRedirect ()->redirectUser ( Redirects::PLEASE_LOGIN_REDIRECT , "Please login before continuing!" );

}


/* Check if the requested action is null  */

$Bearden->getBeardenActionHandler ()->getRequestedAction () !== NULL

    /* Execute the requested action */

    ? $Bearden->getBeardenActionHandler ()->executeRequestedAction ( $Bearden->getBeardenActionHandler ()->getRequestedAction () )

    /* There was no action requested, do nothing */

    : FALSE;

$formAnswers = $Bearden->getBeardenActionHandler ()->getBeardenActions ()->getExitForm ();

/* Check if there was form data returned */

$formAnswers == NULL

    /* There was no form data returned */

    ? $Bearden->getBeardenRedirect ()->redirectUser ( Redirects::FORM_RESULTS_RED_REDIRECT , "The form you requested does not exist!" )


    /* There was from data returned */

    : FALSE;

foreach ( $formAnswers as $form ) {

    /* Make sure the slashes are stripped */

    $form = array_map ( 'stripslashes' , $form );


    ?>



    <!DOCTYPE html>
    <!--[if lt IE 7 ]>
    <html class="ie ie6" lang="en"><![endif]-->
    <!--[if IE 7 ]>
    <html class="ie ie7" lang="en"><![endif]-->
    <!--[if IE 8 ]>
    <html class="ie ie8" lang="en"><![endif]-->
    <!--[if (gte IE 9)|!(IE)]><!-->
    <html lang="en"><!--<![endif]-->
    <head>

        <!-- Basic Page Needs
         ================================================== -->
        <meta charset="utf-8">
        <title>Exit Form Results - <? echo $form[ 'studentID' ] ?></title>

        <!-- Mobile Specific
         ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- CSS
         ================================================== -->
        <link rel="stylesheet" type="text/css" href="/css/style.css">

        <!-- Java Script
         ================================================== -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script src="js/flexslider.js"></script>
        <script src="js/jquery.isotope.min.js"></script>
        <script src="js/custom.js"></script>
        <script src="js/ender.min.js"></script>
        <script src="js/selectnav.js"></script>
        <script src="js/imagebox.min.js"></script>
        <script src="js/carousel.js"></script>
        <script src="js/twitter.js"></script>
        <script src="js/tooltip.js"></script>
        <script src="js/popover.js"></script>
        <script src="js/effects.js"></script>
        <script src="js/bearden.js"></script>


        <style type="text/css">

            .eight {
                margin: 0 auto;
            }

            #college input {

                margin: 0 auto;
            }

            #college th {
                text-align: center;
            }

            #feedback th {

                text-align: center;

            }

            #feedback td {

                text-align: center;
            }

            .sixteen {
                margin-top: 20px;
            }

            table {
                margin-top: 10px;
            }


        </style>
    </head>
    <body>

    <!-- Header -->
    <div id="header">

        <!-- 960 Container Start -->
        <div class="container ie-dropdown-fix">

            <!-- Logo -->
            <div class="four columns">
                <a href="#"><img src="images/logo.png" alt=""/></a>
            </div>

            <!-- Main Navigation Start -->
            <div class="twelve columns">
                <div id="navigation">
                    <ul id="nav">

                        <li><a href="/takeForm?action=cancel" id="current">Cancel Form</a></li>

                    </ul>
                </div>
            </div>
            <!-- Main Navigation End -->

        </div>
        <!-- 960 Container End -->

    </div>
    <!-- End Header -->

    <div id="page-title">

        <!-- 960 Container Start -->
        <div class="container">
            <div class="sixteen columns">
                <h2>Exit Form Results
                    - <? echo $form[ 'firstname' ] . " " . $form[ 'middlename' ] . " " . $form[ 'lastname' ] . " (" . $form[ 'studentID' ] . ")" ?></h2>
            </div>
        </div>
        <!-- 960 Container End -->

    </div>
    <!-- 960 Container Start -->
    <form method="POST" action="/panel" id="formSubmit">


    <div class="container" style="padding-top: 20px;">
    <?

    /*  Check if the message is empty */

    if ( !empty( $_GET[ 'message' ] ) ) {

        /*  Check if the message is GREEN */

        echo isset( $_GET[ 'green' ] )

            /* The message is green, make the HTML markup according  */

            ? '<div class="notification success closeable">'

            /* The message is green, make the HTML markup according  */

            : '<div class="notification error closeable">';

        /*  Now echo the proper message */

        echo '<p><span>' . $_GET[ 'message' ] . '</span></p>';

        /*  End the div for the message box */

        echo '</div>';
    }
    ?>

    <div class="sixteen columns">

        <h4 class="headline"> Personal Information</h4>

    </div>

    <div class="eight columns">

        <table>
            <tr>
                <td>
                    <label><b>*</b> First Name:<br/></label>
                    <input type="text" size="15" name="firstname" value="<? echo $form[ 'firstname' ] ?>"/>
                </td>
            </tr>
        </table>
    </div>
    <div class="eight columns">
        <table>

            <tr>
                <td>
                    <label>Middle Initial:<br/></label>
                    <input type="text" name="middlename" size="3" maxlength="1"
                           value="<? echo $form[ 'middlename' ] ?>"/>
                </td>
            </tr>
        </table>

    </div>

    <div class="eight columns">
        <table>

            <tr>
                <td>
                    <label><b>*</b> Last Name:<br/></label>
                    <input type="text" name="lastname" size="15" maxlength="35" value="<? echo $form[ 'lastname' ] ?>"/>
                </td>
            </tr>
        </table>

    </div>

    <div class="eight columns">
        <table>

            <tr>
                <td>
                    <label> <b>*</b>Street Mailing Address:<br/></label>
                    <input type="text" name="address" size="35" maxlength="255" value="<? echo $form[ 'address' ] ?>"/>
                </td>
            </tr>
        </table>

    </div>

    <div class="eight columns">
        <table>

            <tr>
                <td>
                    <label><b>*</b> City:<br/></label>
                    <input type="text" name="city" size="20" maxlength="100" value="<? echo $form[ 'city' ] ?>"/>
                </td>
            </tr>
        </table>

    </div>

    <div class="eight columns">
        <table>

            <tr>
                <td>
                    <label><b>*</b> Zip Code:<br/></label>
                    <input type="text" name="zipcode" size="25" maxlength="255" value="<? echo $form[ 'zipcode' ] ?>"/>
                </td>
            </tr>
        </table>

    </div>

    <div class="eight columns">
        <table>

            <tr>
                <td>
                    <label><b>*</b>Email address:<br/></label>
                    <input type="text" name="email" size="25" maxlength="255" value="<? echo $form[ 'email' ] ?>"/>
                </td>
            </tr>
        </table>
    </div>

    <div class="eight columns">
        <table>

            <tr>
                <td>
                    <label><b>*</b>Student Cell Number<br/></label>
                    <input type="text" name="studentcell" size="12" maxlength="255"
                           value="<? echo $form[ 'studentcell' ] ?>"/>
                </td>
            </tr>
        </table>
    </div>

    <div class="sixteen columns">
        <h4 class="headline">Military</h4>

    </div>
    <div class="six columns">
        <table>
            <tr>
                <td>
                    <strong>
                        <p style="font-size: small;">If you plan to go into the military, please tell us:</p>
                    </strong>

                    <label><strong>Have you met with a recruiter yet?</strong></label>
                </td>
            </tr>
            <tr>
                <td>

                    <label><input type="radio" name="recruiter"
                                  value="Yes" <? echo strcasecmp ( $form[ 'recruiter' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>Yes</label>
                    <label><input type="radio" name="recruiter"
                                  value="No" <? echo strcasecmp ( $form[ 'recruiter' ] , "No" ) == 0 ? 'checked' : ''; ?>/>No</label>
                    <label><input type="radio" name="recruiter"
                                  value="Not Applicable" <? echo strcasecmp ( $form[ 'recruiter' ] , "Not Applicable" ) == 0 ? 'checked' : ''; ?>/>Not
                        Applicable</label>

                </td>
            </tr>
        </table>

    </div>


    <div class="six columns">
        <label><strong>Branch of Service (select one)</strong><br/></label>
        <table>
            <tr>
                <td>
                    <select name="branch">
                        <option
                            value="none" <? echo strcasecmp ( $form[ 'branch' ] , "none" ) == 0 ? 'selected' : ''; ?>>
                            Select
                        </option>
                        <option
                            value="Army" <? echo strcasecmp ( $form[ 'branch' ] , "Army" ) == 0 ? 'selected' : ''; ?>>
                            Army
                        </option>
                        <option
                            value="Air Force" <? echo strcasecmp ( $form[ 'branch' ] , "Air Force" ) == 0 ? 'selected' : ''; ?>>
                            Air Force
                        </option>
                        <option
                            value="Navy" <? echo strcasecmp ( $form[ 'branch' ] , "Navy" ) == 0 ? 'selected' : ''; ?>>
                            Navy
                        </option>
                        <option
                            value="Marine Corps" <? echo strcasecmp ( $form[ 'branch' ] , "Marine Corps" ) == 0 ? 'selected' : ''; ?>>
                            Marine Corps
                        </option>
                        <option
                            value="Coast Guard" <? echo strcasecmp ( $form[ 'branch' ] , "Coast Guard" ) == 0 ? 'selected' : ''; ?>>
                            Coast Guard
                        </option>
                        <option
                            value="Police (local Gov't)" <? echo strcasecmp ( $form[ 'branch' ] , "Police (local Gov't)" ) == 0 ? 'selected' : ''; ?>>
                            Police (local Gov't)
                        </option>
                        <option
                            value="Fire (local Gov't)" <? echo strcasecmp ( $form[ 'branch' ] , "Fire (local Gov't)" ) == 0 ? 'selected' : ''; ?>>
                            Fire (local Gov't)
                        </option>
                        <option value="EMT" <? echo strcasecmp ( $form[ 'branch' ] , "EMT" ) == 0 ? 'selected' : ''; ?>>
                            EMT
                        </option>
                        <option
                            value="Other" <? echo strcasecmp ( $form[ 'branch' ] , "Other" ) == 0 ? 'selected' : ''; ?>>
                            Other
                        </option>
                    </select>
                </td>
            </tr>

        </table>
    </div>

    <div class="four columns">
        <label><strong>Expected starting date:</strong><br/></label>

        <table>
            <tr>
                <td>
                    <input type="text" name="startingdate" size="10" maxlength="10"
                           value="<? echo $form[ 'startingdate' ] ?>"/>

                </td>
            </tr>
        </table>
    </div>

    <div class="sixteen columns">
        <h4 class="headline"> College and Workforce</h4>

    </div>

    <div class="eight columns">
        <label><b>*</b><strong> Do you plan to participate in commencement (graduation ceremony)?</strong><br/></label>
        <table>
            <tr>
                <td>
                    <label><input type="radio" name="participate"
                                  value="Yes" <? echo strcasecmp ( $form[ 'participate' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>Yes</label>
                </td>
            </tr>
            <tr>
                <td>
                    <label><input type="radio" name="participate"
                                  value="No" <? echo strcasecmp ( $form[ 'participate' ] , "No" ) == 0 ? 'checked' : ''; ?>>No</label>
                </td>
            </tr>
        </table>

    </div>
    <div class="sixteen columns">
        <label><b>*</b><strong> My plans after graduation are:</strong><br/></label>
        <table>
            <tr>
                <td>

                    <select name="graduationplans">
                        <option
                            value="Go into the military" <? echo strcasecmp ( $form[ 'graduationplans' ] , "Go into the military" ) == 0 ? 'selected' : ''; ?>>
                            Go into the military
                        </option>
                        <option
                            value="Go to work" <? echo strcasecmp ( $form[ 'graduationplans' ] , "Go to work" ) == 0 ? 'selected' : ''; ?>>
                            Go to work
                        </option>
                        <option
                            value="Attend a technical or vocational school" <? echo strcasecmp ( $form[ 'graduationplans' ] , "Attend a technical or vocational school" ) == 0 ? 'selected' : ''; ?>>
                            Attend a technical or vocational
                            school
                        </option>
                        <option
                            value="Attend a 2 year community college" <? echo strcasecmp ( $form[ 'graduationplans' ] , "Attend a 2 year community college" ) == 0 ? 'selected' : ''; ?>>
                            Attend a 2 year community college
                        </option>
                        <option
                            value="Attend a 4 year college or university" <? echo strcasecmp ( $form[ 'graduationplans' ] , "Attend a 4 year college or university" ) == 0 ? 'selected' : ''; ?>>
                            Attend a 4 year college or university
                        </option>
                    </select>
                </td>
            </tr>

        </table>

    </div>


    <div class="sixteen columns">
        <label><strong>If planning to enroll in a 2 year community college please choose which
                one</strong></label>
        <table>
            <tr>
                <td>

                    <select name="2yearcollege">
                        <option
                            value="Pellissippi State Community College" <? echo strcasecmp ( $form[ '2yearcollege' ] , "Pellissippi State Community College" ) == 0 ? 'selected' : ''; ?>>
                            Pellissippi State Community College
                        </option>
                        <option
                            value="Roane State Community College" <? echo strcasecmp ( $form[ '2yearcollege' ] , "Roane State Community College" ) == 0 ? 'selected' : ''; ?>>
                            Roane State Community College
                        </option>
                        <option
                            value="Walters State Community College" <? echo strcasecmp ( $form[ '2yearcollege' ] , "Walters State Community College" ) == 0 ? 'selected' : ''; ?>>
                            Walters State Community College
                        </option>
                        <option
                            value="Other" <? echo strcasecmp ( $form[ '2yearcollege' ] , "Other" ) == 0 ? 'selected' : ''; ?>>
                            Other
                        </option>
                    </select>
                </td>
            </tr>
        </table>
    </div>

    <div class="sixteen columns">
        <label><strong>If you plan to work after graduation and NOT attend college in the fall
                of <? echo date ( "Y" ) + 1 ?>,
                please tell us where.</strong></label>
        <table>
            <tr>
                <td>
                    <input type="text" name="workforcegrad" size="25" maxlength="255"
                           value="<? echo $form[ 'workforcegrad' ] ?>"/>
                </td>
            </tr>
        </table>
    </div>

    <div class="sixteen columns">

        <h4 class="headline">College Admission</h4>

    </div>

    <div class="sixteen columns">
        <table>
            <tr>
                <td>
                    <strong>
                        <p>
                            Please list the college to which you were admitted and will be attending. If you have not
                            made a
                            final decision please list your second choice. FINAL TRANSCRIPTS will only be sent to the
                            school/schools listed. (Remember, a final transcript is required to register for college
                            classes.)
                            Don't forget to request a transcript from Pellissippi if you took a Dual Enrollment class in
                            high school!
                        </p>
                    </strong>
                </td>
            </tr>
        </table>
    </div>


    <div class="sixteen columns">

        <table>
            <tr>
                <td>
                    <a href="http://www.actstudent.org/scores/scodes.html" class="link" target="_blank">Link to find ACT
                        codes for Schools. </a>
                </td>
            </tr>
            <tr>

        </table>
    </div>

    <div class="sixteen columns">

        <table id="college" class="standard-table">

            <tr>
                <th></th>
                <th>ACT Code</th>
                <th>School Name</th>
                <th>School City and State</th>
            </tr>

            <tr>
                <td><label>Send my 1st transcript to:</label></td>
                <td><input align="center" type="text" name="college-1-1" size="24" maxlength="255"
                           value="<? echo $form[ 'college-1-1' ] ?>"/></td>
                <td><input align="center" type="text" name="college-1-2" size="24" maxlength="255"
                           value="<? echo $form[ 'college-1-2' ] ?>"/></td>
                <td><input align="center" type="text" name="college-1-3" size="24" maxlength="255"
                           value="<? echo $form[ 'college-1-3' ] ?>"/></td>
            </tr>

            <tr>

                <td>Send my 2nd transcript to:</td>
                <td><input type="text" name="college-2-1" size="24" maxlength="255"
                           value="<? echo $form[ 'college-2-1' ] ?>"/></td>
                <td><input type="text" name="college-2-2" size="24" maxlength="255"
                           value="<? echo $form[ 'college-2-2' ] ?>"/></td>
                <td><input type="text" name="college-2-3" size="24" maxlength="255"
                           value="<? echo $form[ 'college-2-3' ] ?>"/></td>
            </tr>

        </table>
    </div>


    <div class="sixteen columns">
        <label><b>*</b><strong>If you will play college sports you MUST check here to have a final transcript sent to
                NCAA
                or NAIA?</strong><br/></label>
        <table>
            <tr>
                <td>
                    <label><input type="radio" name="sportrans"
                                  value="NCAA" <? echo strcasecmp ( $form[ 'sportrans' ] , "NCAA" ) == 0 ? 'checked' : ''; ?>/>NCAA</label>
                </td>
            </tr>
            <tr>
                <td>
                    <label><input type="radio" name="sportrans"
                                  value="NAIA" <? echo strcasecmp ( $form[ 'sportrans' ] , "NAIA" ) == 0 ? 'checked' : ''; ?>/>NAIA</label>
                </td>
            </tr>
            <tr>
                <td>
                    <label><input type="radio" name="sportrans"
                                  value="Not Applicable" <? echo strcasecmp ( $form[ 'sportrans' ] , "Not Applicable" ) == 0 ? 'checked' : ''; ?>/>Not
                        Applicable</label></td>
            </tr>
        </table>
    </div>

    <div class="sixteen columns">
        <label><b>*</b><strong>If you do not want any transcripts sent at the end of the school year, please check
                here.</strong><br/></label>
        <table>
            <tr>
                <td>

                    <label>
                        <input type="hidden" name="notrans" value="No"/>

                        <input type="checkbox" name="notrans"
                               value="Yes" <? echo strcasecmp ( $form[ 'notrans' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>No
                        Transcript</label>
                </td>
            </tr>

        </table>

    </div>

    <div class="sixteen columns">
        <label><b>*</b><strong>Please list additional colleges where you were offered admission, but have chosen not to
                attend.

                <table id="college" class="standard-table">

                    <tr>
                        <th></th>
                        <th>School Name</th>
                        <th>School City and State</th>
                    </tr>

                    <tr>
                        <td><label>Additional College:</label></td>
                        <td><input align="center" type="text" name="add-1-1" size="24" maxlength="255"
                                   value="<? echo $form[ 'add-1-1' ] ?>"/></td>
                        <td><input align="center" type="text" name="add-1-2" size="24" maxlength="255"
                                   value="<? echo $form[ 'add-1-2' ] ?>"/></td>
                    </tr>

                    <tr>
                        <td><label>Additional College:</label></td>
                        <td><input align="center" type="text" name="add-2-1" size="24" maxlength="255"
                                   value="<? echo $form[ 'add-2-1' ] ?>"/></td>
                        <td><input align="center" type="text" name="add-2-2" size="24" maxlength="255"
                                   value="<? echo $form[ 'add-2-2' ] ?>"/></td>
                    </tr>
                    <tr>
                        <td><label>Additional College:</label></td>
                        <td><input align="center" type="text" name="add-3-1" size="24" maxlength="255"
                                   value="<? echo $form[ 'add-3-1' ] ?>"/></td>
                        <td><input align="center" type="text" name="add-3-2" size="24" maxlength="255"
                                   value="<? echo $form[ 'add-3-2' ] ?>"/></td>
                    </tr>
                    <tr>
                        <td><label>Additional College:</label></td>
                        <td><input align="center" type="text" name="add-4-1" size="24" maxlength="255"
                                   value="<? echo $form[ 'add-4-1' ] ?>"/></td>
                        <td><input align="center" type="text" name="add-4-2" size="24" maxlength="255"
                                   value="<? echo $form[ 'add-4-2' ] ?>"/></td>
                    </tr>
                    <tr>
                        <td><label>Additional College:</label></td>
                        <td><input align="center" type="text" name="add-5-1" size="24" maxlength="255"
                                   value="<? echo $form[ 'add-5-1' ] ?>"/></td>
                        <td><input align="center" type="text" name="add-5-2" size="24" maxlength="255"
                                   value="<? echo $form[ 'add-5-2' ] ?>"/></td>
                    </tr>
                    <tr>
                        <td><label>Additional College:</label></td>
                        <td><input align="center" type="text" name="add-6-1" size="24" maxlength="255"
                                   value="<? echo $form[ 'add-6-1' ] ?>"/></td>
                        <td><input align="center" type="text" name="add-6-2" size="24" maxlength="255"
                                   value="<? echo $form[ 'add-6-2' ] ?>"/></td>
                    </tr>
                </table>
    </div>

    <div class="sixteen columns">

        <h4 class="headline">Scholarships</h4>


    </div>


    <div class="sixteen columns">
        <label><b>*</b><strong>Did you qualify for the Tennessee Lottery HOPE Scholarship? (Unweighted 3.0 GPA OR 21
                ACT)?</strong><br/></label>
        <table>
            <tr>
                <td>
                    <label><input type="radio" name="tnhope"
                                  value="Yes" <? echo strcasecmp ( $form[ 'tnhope' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>Yes</label>
                </td>
            </tr>
            <tr>
                <td>
                    <label><input type="radio" name="tnhope"
                                  value="No" <? echo strcasecmp ( $form[ 'tnhope' ] , "No" ) == 0 ? 'checked' : ''; ?>/>No</label>
                </td>
            </tr>
        </table>

    </div>
    <div class="sixteen columns">
        <label><b>*</b><strong>Did you qualify for additional Tennessee Lottery HOPE Scholarships?</strong><br/></label>
        <table>
            <tr>
                <td>
                    <label>

                        <input type="hidden" name="genassemb" value="No"/>

                        <input type="checkbox" name="genassemb"
                               value="Yes" <? echo strcasecmp ( $form[ 'genassemb' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>HOPE
                        General Assembly Merit (GAMS)? (29
                        ACT
                        & 3.75 GPA)</label>
                </td>
            </tr>
            <tr>
                <td>
                    <label>
                        <input type="hidden" name="aspireward" value="No"/>

                        <input type="checkbox" name="aspireward"
                               value="Yes" <? echo strcasecmp ( $form[ 'aspireward' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>HOPE
                        Aspire Award (family income
                        $36,000/year
                        or less)</label>
                </td>
            </tr>
        </table>

    </div>

    <div class="sixteen columns">
        <label><b>*</b><strong>Were you offered the tnAchieves Scholarship (to attend community
                college)??</strong><br/></label>
        <table>
            <tr>
                <td>
                    <label><input type="radio" name="tnachieve"
                                  value="Yes" <? echo strcasecmp ( $form[ 'tnachieve' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>Yes</label>
                </td>
            </tr>
            <tr>
                <td>
                    <label><input type="radio" name="tnachieve"
                                  value="No"  <? echo strcasecmp ( $form[ 'tnachieve' ] , "No" ) == 0 ? 'checked' : ''; ?>/>No</label>
                </td>
            </tr>
        </table>

    </div>
    <div class="sixteen columns">
        <label><b>*</b><strong>If you were offered scholarships to the college you will be attending (or from other
                sources), please list the information below: (scholarships you plan to ACCEPT should go here)
                <table id="college" class="standard-table">

                    <tr>
                        <th></th>
                        <th>Scholarship Name</th>
                        <th>Offered By</th>
                        <th>Renewable how many years?</th>
                        <th>Amount per Yr</th>
                        <th>Accepted / Declined</th>

                    </tr>

                    <tr>
                        <td><label>Scholarship #1</label></td>
                        <td><input align="center" type="text" name="acc-1-1" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-1-1' ] ?>"/></td>
                        <td><input align="center" type="text" name="acc-1-2" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-1-2' ] ?>"/></td>
                        <td><input align="center" type="text" name="acc-1-3" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-1-3' ] ?>"/></td>
                        <td><input align="center" type="text" name="acc-1-4" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-1-4' ] ?>"/></td>
                        <td><input align="center" type="text" name="acc-1-5" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-1-5' ] ?>"/></td>

                    </tr>

                    <tr>
                        <td><label>Scholarship #2</label></td>
                        <td><input align="center" type="text" name="acc-2-1" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-2-1' ] ?>"/></td>
                        <td><input align="center" type="text" name="acc-2-2" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-2-2' ] ?>"/></td>
                        <td><input align="center" type="text" name="acc-2-3" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-2-3' ] ?>"/></td>
                        <td><input align="center" type="text" name="acc-2-4" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-2-4' ] ?>"/></td>
                        <td><input align="center" type="text" name="acc-2-5" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-2-5' ] ?>"/></td>

                    </tr>
                    <tr>
                        <td><label>Scholarship #3</label></td>
                        <td><input align="center" type="text" name="acc-3-1" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-3-1' ] ?>"/></td>
                        <td><input align="center" type="text" name="acc-3-2" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-3-2' ] ?>"/></td>
                        <td><input align="center" type="text" name="acc-3-3" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-3-3' ] ?>"/></td>
                        <td><input align="center" type="text" name="acc-3-4" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-3-4' ] ?>"/></td>
                        <td><input align="center" type="text" name="acc-3-5" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-3-5' ] ?>"/></td>

                    </tr>
                    <tr>
                        <td><label>Scholarship #4</label></td>
                        <td><input align="center" type="text" name="acc-4-1" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-4-1' ] ?>"/></td>
                        <td><input align="center" type="text" name="acc-4-2" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-4-2' ] ?>"/></td>
                        <td><input align="center" type="text" name="acc-4-3" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-4-3' ] ?>"/></td>
                        <td><input align="center" type="text" name="acc-4-4" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-4-4' ] ?>"/></td>
                        <td><input align="center" type="text" name="acc-4-5" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-4-5' ] ?>"/></td>

                    </tr>
                    <tr>
                        <td><label>Scholarship #5</label></td>
                        <td><input align="center" type="text" name="acc-5-1" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-5-1' ] ?>"/></td>
                        <td><input align="center" type="text" name="acc-5-2" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-5-2' ] ?>"/></td>
                        <td><input align="center" type="text" name="acc-5-3" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-5-3' ] ?>"/></td>
                        <td><input align="center" type="text" name="acc-5-4" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-5-4' ] ?>"/></td>
                        <td><input align="center" type="text" name="acc-5-5" size="24" maxlength="255"
                                   value="<? echo $form[ 'acc-5-5' ] ?>"/></td>

                    </tr>
                </table>
    </div>
    <div class="sixteen columns">
        <label><b>*</b><strong>If you were offered scholarships from other colleges or sources (even if they were not
                accepted) please list the information below: (Scholarships you chose NOT TO ACCEPT should go here)
                <table id="college" class="standard-table">

                    <tr>
                        <th></th>
                        <th>Scholarship Name</th>
                        <th>Offered By</th>
                        <th>Renewable how many years?</th>
                        <th>Amount per Yr</th>
                        <th>Accepted / Declined</th>

                    </tr>

                    <tr>
                        <td><label>Scholarship #1</label></td>
                        <td><input align="center" type="text" name="dec-1-1" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-1-1' ] ?>"/></td>
                        <td><input align="center" type="text" name="dec-1-2" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-1-2' ] ?>"/></td>
                        <td><input align="center" type="text" name="dec-1-3" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-1-3' ] ?>"/></td>
                        <td><input align="center" type="text" name="dec-1-4" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-1-4' ] ?>"/></td>
                        <td><input align="center" type="text" name="dec-1-5" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-1-5' ] ?>"/></td>

                    </tr>

                    <tr>
                        <td><label>Scholarship #2</label></td>
                        <td><input align="center" type="text" name="dec-2-1" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-2-1' ] ?>"/></td>
                        <td><input align="center" type="text" name="dec-2-2" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-2-2' ] ?>"/></td>
                        <td><input align="center" type="text" name="dec-2-3" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-2-3' ] ?>"/></td>
                        <td><input align="center" type="text" name="dec-2-4" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-2-4' ] ?>"/></td>
                        <td><input align="center" type="text" name="dec-2-5" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-2-5' ] ?>"/></td>

                    </tr>
                    <tr>
                        <td><label>Scholarship #3</label></td>
                        <td><input align="center" type="text" name="dec-3-1" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-3-1' ] ?>"/></td>
                        <td><input align="center" type="text" name="dec-3-2" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-3-2' ] ?>"/></td>
                        <td><input align="center" type="text" name="dec-3-3" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-3-3' ] ?>"/></td>
                        <td><input align="center" type="text" name="dec-3-4" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-3-4' ] ?>"/></td>
                        <td><input align="center" type="text" name="dec-3-5" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-3-5' ] ?>"/></td>

                    </tr>
                    <tr>
                        <td><label>Scholarship #4</label></td>
                        <td><input align="center" type="text" name="dec-4-1" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-4-1' ] ?>"/></td>
                        <td><input align="center" type="text" name="dec-4-2" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-4-2' ] ?>"/></td>
                        <td><input align="center" type="text" name="dec-4-3" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-4-3' ] ?>"/></td>
                        <td><input align="center" type="text" name="dec-4-4" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-4-4' ] ?>"/></td>
                        <td><input align="center" type="text" name="dec-4-5" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-4-5' ] ?>"/></td>

                    </tr>
                    <tr>
                        <td><label>Scholarship #5</label></td>
                        <td><input align="center" type="text" name="dec-5-1" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-5-1' ] ?>"/></td>
                        <td><input align="center" type="text" name="dec-5-2" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-5-2' ] ?>"/></td>
                        <td><input align="center" type="text" name="dec-5-3" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-5-3' ] ?>"/></td>
                        <td><input align="center" type="text" name="dec-5-4" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-5-4' ] ?>"/></td>
                        <td><input align="center" type="text" name="dec-5-5" size="24" maxlength="255"
                                   value="<? echo $form[ 'dec-5-5' ] ?>"/></td>

                    </tr>
                </table>
    </div>
    <div class="sixteen columns">

        <h4 class="headline">Bearden School Counseling Program Input</h4>


    </div>


    <div class="five columns">
        <label><b>*</b> <strong>Would you like to meet with your counselor to discuss future plans?</strong></label>
        <table>
            <tr>
                <td>
                    <label><input type="radio" name="meetwith"
                                  value="Yes"  <? echo strcasecmp ( $form[ 'meetwith' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>Yes</label>
                </td>
            </tr>
            <tr>
                <td>
                    <label><input type="radio" name="meetwith" value="No"
                            <? echo strcasecmp ( $form[ 'meetwith' ] , "No" ) == 0 ? 'checked' : ''; ?>/>No</label>
                </td>
            </tr>
        </table>
    </div>


    <div class="five columns">
        <label><b>*</b> <strong>Do you feel your counselor treated you with kindness, respect, and
                dignity?</strong></label>
        <table>
            <tr>
                <td>
                    <select name="kindness">
                        <option
                            value="Yes" <? echo strcasecmp ( $form[ 'kindness' ] , "Yes" ) == 0 ? 'selected' : ''; ?>>
                            Yes
                        </option>
                        <option value="No" <? echo strcasecmp ( $form[ 'kindness' ] , "No" ) == 0 ? 'selected' : ''; ?>>
                            No
                        </option>
                    </select>
                </td>
            </tr>
        </table>

    </div>

    <div class="sixteen columns">
        <label><b>*</b> <strong>Please provide your feedback to help us shape and improve BHS Guidance
                services:</strong></label>

        <table class="standard-table" id="feedback">
            <tr>
                <th>&nbsp;</th>
                <th>Never</th>
                <th>Sometimes</th>
                <th>Mostly</th>
                <th>Always</th>
                <th>Not Applicable</th>
            </tr>
            <tr>
                <td>I felt welcome in the counseling office</td>
                <td><input type="radio" name="question-1-1"
                           value="Never" <? echo strcasecmp ( $form[ 'question-1-1' ] , "Never" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="question-1-1"
                           value="Sometimes" <? echo strcasecmp ( $form[ 'question-1-1' ] , "Sometimes" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="question-1-1"
                           value="Mostly" <? echo strcasecmp ( $form[ 'question-1-1' ] , "Mostly" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="question-1-1"
                           value="Always" <? echo strcasecmp ( $form[ 'question-1-1' ] , "Always" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="question-1-1"
                           value="Not Applicable" <? echo strcasecmp ( $form[ 'question-1-1' ] , "Not Applicable" ) == 0 ? 'checked' : ''; ?>/>
                </td>
            </tr>
            <tr>
                <td>I felt comfortable talking with my counselor</td>
                <td><input type="radio" name="question-2-1"
                           value="Never"  <? echo strcasecmp ( $form[ 'question-2-1' ] , "Never" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="question-2-1"
                           value="Sometimes"  <? echo strcasecmp ( $form[ 'question-2-1' ] , "Sometimes" ) == 0 ? 'checked' : ''; ?>>
                </td>
                <td><input type="radio" name="question-2-1"
                           value="Mostly"  <? echo strcasecmp ( $form[ 'question-2-1' ] , "Mostly" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="question-2-1"
                           value="Always"  <? echo strcasecmp ( $form[ 'question-2-1' ] , "Always" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="question-2-1" value="Not Applicable"
                        <? echo strcasecmp ( $form[ 'question-2-1' ] , "Not Applicable" ) == 0 ? 'checked' : ''; ?>/>
                </td>
            </tr>
            <tr>
                <td>How available was your counselor?</td>
                <td><input type="radio" name="question-3-1"
                           value="Never" <? echo strcasecmp ( $form[ 'question-3-1' ] , "Never" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="question-3-1"
                           value="Sometimes" <? echo strcasecmp ( $form[ 'question-3-1' ] , "Sometimes" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="question-3-1"
                           value="Mostly" <? echo strcasecmp ( $form[ 'question-3-1' ] , "Mostly" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="question-3-1"
                           value="Always" <? echo strcasecmp ( $form[ 'question-3-1' ] , "Always" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="question-3-1"
                           value="Not Applicable" <? echo strcasecmp ( $form[ 'question-3-1' ] , "Not Applicable" ) == 0 ? 'checked' : ''; ?>/>
                </td>
            </tr>
        </table>

    </div>


    <div class="sixteen columns">
        <label><b>*</b>
            <strong>
                Please rank the effectiveness and level of caring of your counselor.&nbsp; (note: a 1
                ranking = not applicable; a&nbsp;10 ranking is the highest)
            </strong>
        </label>

        <table class="standard-table" id="feedback">
            <tr>
                <th>&nbsp;</th>
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th>5</th>
                <th>6</th>
                <th>7</th>
                <th>8</th>
                <th>9</th>
                <th>10</th>
            </tr>
            <tr>
                <td>Counselor assistance for academic issues:</td>
                <td><input type="radio" name="academic_issues"
                           value="1" <? echo strcasecmp ( $form[ 'academic_issues' ] , "1" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="academic_issues"
                           value="2" <? echo strcasecmp ( $form[ 'academic_issues' ] , "2" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="academic_issues"
                           value="3" <? echo strcasecmp ( $form[ 'academic_issues' ] , "3" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="academic_issues"
                           value="4" <? echo strcasecmp ( $form[ 'academic_issues' ] , "4" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="academic_issues"
                           value="5" <? echo strcasecmp ( $form[ 'academic_issues' ] , "5" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="academic_issues"
                           value="6" <? echo strcasecmp ( $form[ 'academic_issues' ] , "6" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="academic_issues"
                           value="7" <? echo strcasecmp ( $form[ 'academic_issues' ] , "7" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="academic_issues"
                           value="8" <? echo strcasecmp ( $form[ 'academic_issues' ] , "8" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="academic_issues"
                           value="9" <? echo strcasecmp ( $form[ 'academic_issues' ] , "9" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="academic_issues"
                           value="10" <? echo strcasecmp ( $form[ 'academic_issues' ] , "10" ) == 0 ? 'checked' : ''; ?>/>
                </td>
            </tr>
            <tr>
                <td>Counselor assistance for college/career planning:</td>
                <td><input type="radio" name="career_planning"
                           value="1" <? echo strcasecmp ( $form[ 'career_planning' ] , "1" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="career_planning"
                           value="2"  <? echo strcasecmp ( $form[ 'career_planning' ] , "2" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="career_planning"
                           value="3"  <? echo strcasecmp ( $form[ 'career_planning' ] , "3" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="career_planning"
                           value="4"  <? echo strcasecmp ( $form[ 'career_planning' ] , "4" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="career_planning"
                           value="5"  <? echo strcasecmp ( $form[ 'career_planning' ] , "5" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="career_planning"
                           value="6"  <? echo strcasecmp ( $form[ 'career_planning' ] , "6" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="career_planning"
                           value="7"  <? echo strcasecmp ( $form[ 'career_planning' ] , "7" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="career_planning"
                           value="8"  <? echo strcasecmp ( $form[ 'career_planning' ] , "8" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="career_planning"
                           value="9"  <? echo strcasecmp ( $form[ 'career_planning' ] , "9" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="career_planning"
                           value="10"  <? echo strcasecmp ( $form[ 'career_planning' ] , "10" ) == 0 ? 'checked' : ''; ?>/>
                </td>
            </tr>

            <tr>
                <td>Counselor assistance for personal/social needs:</td>
                <td><input type="radio" name="social_needs"
                           value="1" <? echo strcasecmp ( $form[ 'social_needs' ] , "1" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="social_needs"
                           value="2"  <? echo strcasecmp ( $form[ 'social_needs' ] , "2" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="social_needs"
                           value="3"  <? echo strcasecmp ( $form[ 'social_needs' ] , "3" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="social_needs"
                           value="4"  <? echo strcasecmp ( $form[ 'social_needs' ] , "4" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="social_needs"
                           value="5"  <? echo strcasecmp ( $form[ 'social_needs' ] , "5" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="social_needs"
                           value="6"  <? echo strcasecmp ( $form[ 'social_needs' ] , "6" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="social_needs"
                           value="7"  <? echo strcasecmp ( $form[ 'social_needs' ] , "7" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="social_needs"
                           value="8"  <? echo strcasecmp ( $form[ 'social_needs' ] , "8" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="social_needs"
                           value="9"  <? echo strcasecmp ( $form[ 'social_needs' ] , "9" ) == 0 ? 'checked' : ''; ?>/>
                </td>
                <td><input type="radio" name="social_needs"
                           value="10"  <? echo strcasecmp ( $form[ 'social_needs' ] , "10" ) == 0 ? 'checked' : ''; ?>/>
                </td>
            </tr>
        </table>

    </div>

    <div class="sixteen columns">

        <table class="standard-table">
            <tr>
                <th>Indicate which events you, your parent, or guardian have attended or plan to attend [Check all
                    that
                    apply]
                </th>

            </tr>
            <tr>
                <td>
                    <label>
                        <input type="hidden" name="event1" value="No"/>

                        <input type="checkbox" name="event1"
                               value="Yes" <? echo strcasecmp ( $form[ 'event1' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>
                        College Game On during Dawg Day
                    </label>
                </td>
            </tr>
            <tr>
                <td><label>
                        <input type="hidden" name="event2" value="No"/>
                        <input type="checkbox" name="event2"
                               value="Yes" <? echo strcasecmp ( $form[ 'event2' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>College
                        Cafe
                        during early release</label></td>
            </tr>
            <tr>
                <td><label>
                        <input type="hidden" name="event3" value="No"/>
                        <input type="checkbox" name="event3"
                               value="Yes" <? echo strcasecmp ( $form[ 'event3' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>Open
                        House grade-level meetings (in auditorium or library prior to open house)</label></td>
            </tr>
            <tr>
                <td><label>
                        <input type="hidden" name="event4" value="event4"/>
                        <input type="checkbox" name="event4"
                               value="Yes" <? echo strcasecmp ( $form[ 'event4' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>Financial
                        Aid Workshop (FAFSA Session in December)</label></td>
            </tr>
            <tr>
                <td><label>
                        <input type="hidden" name="event5" value="No"/>
                        <input type="checkbox" name="event5"
                               value="Yes" <? echo strcasecmp ( $form[ 'event5' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>College
                        Workshops
                        (Fall 2012)</label></td>
            </tr>
            <tr>
                <td><label>
                        <input type="hidden" name="event6" value="No"/>
                        <input type="checkbox" name="event6"
                               value="Yes" <? echo strcasecmp ( $form[ 'event6' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>BHS/FHS/HVA
                        College Fair (October each year)</label></td>
            </tr>
            <tr>
                <td><label>
                        <input type="hidden" name="event7" value="No"/>
                        <input type="checkbox" name="event7"
                               value="Yes" <? echo strcasecmp ( $form[ 'event7' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>Frequented
                        the
                        BHS Guidance website</label></td>
            </tr>
            <tr>
                <td><label>
                        <input type="hidden" name="event8" value="No"/>
                        <input type="checkbox" name="event8"
                               value="Yes" <? echo strcasecmp ( $form[ 'event8' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>Met
                        with
                        counselor
                        or
                        staff in the College & Career Center</label></td>
            </tr>
            <tr>
                <td><label>
                        <input type="hidden" name="event9" value="No"/>
                        <input type="checkbox" name="event9"
                               value="Yes" <? echo strcasecmp ( $form[ 'event9' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>Met
                        with
                        school
                        counselor to review graduation requirements</label></td>
            </tr>
            <tr>
                <td><label>
                        <input type="hidden" name="event10" value="No"/>

                        <input type="checkbox" name="event10"
                               value="Yes" <? echo strcasecmp ( $form[ 'event10' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>None
                        of the
                        above</label>
                </td>
            </tr>
        </table>


    </div>
    <div class="sixteen columns">


        <table class="standard-table">
            <tr>
                <th> Indicate areas you would have liked more assistance [Check all that apply]</th>
            </tr>

            <tr>
                <td>
                    <label>

                        <input type="hidden" name="more_assistance1" value="No"/>
                        <input type="checkbox" name="more_assistance1"
                               value="Yes" <? echo strcasecmp ( $form[ 'more_assistance1' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>Personal
                        - Grief
                    </label>
                </td>
            </tr>
            <tr>

                <td>
                    <label>
                        <input type="hidden" name="more_assistance2" value="No"/>
                        <input type="checkbox" name="more_assistance2"
                               value="Yes" <? echo strcasecmp ( $form[ 'more_assistance2' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>Personal
                        -
                        Conflict
                    </label>
                </td>
            </tr>
            <tr>
                <td><label>
                        <input type="hidden" name="more_assistance3" value="No"/>
                        <input type="checkbox" name="more_assistance3"
                               value="Yes" <? echo strcasecmp ( $form[ 'more_assistance3' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>Personal
                        -
                        Personal Issues </label></td>
            <tr>

                <td><label>
                        <input type="hidden" name="more_assistance4" value="No"/>

                        <input type="checkbox" name="more_assistance4"
                               value="Yes" <? echo strcasecmp ( $form[ 'more_assistance4' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>Academic
                        - Test Taking / Study Skills </label></td>
            </tr>
            <tr>
                <td><label>
                        <input type="hidden" name="more_assistance5" value="No"/>
                        <input type="checkbox" name="more_assistance5"
                               value="Yes" <? echo strcasecmp ( $form[ 'more_assistance5' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>Academic
                        -
                        Course Advising </label></td>
            </tr>
            <tr>
                <td><label>
                        <input type="hidden" name="more_assistance6" value="No"/>
                        <input type="checkbox" name="more_assistance6"
                               value="Yes" <? echo strcasecmp ( $form[ 'more_assistance6' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>Academic
                        -
                        Time Management </label></td>
            </tr>
            <tr>
                <td><label>
                        <input type="hidden" name="more_assistance7" value="No"/>
                        <input type="checkbox" name="more_assistance7"
                               value="Yes" <? echo strcasecmp ( $form[ 'more_assistance7' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>Career
                        Planning </label>
                </td>
            </tr>
            <tr>
                <td><label>
                        <input type="hidden" name="more_assistance8" value="No"/>
                        <input type="checkbox" name="more_assistance8"
                               value="Yes" <? echo strcasecmp ( $form[ 'more_assistance8' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>College
                        Planning </label>
                </td>
            </tr>
            <tr>
                <td><label>
                        <input type="hidden" name="more_assistance9" value="No"/>
                        <input type="checkbox" name="more_assistance9"
                               value="Yes" <? echo strcasecmp ( $form[ 'more_assistance9' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>Career
                        - Financial Aid / Scholarships </label></td>
            </tr>
            <tr>
                <td><label>
                        <input type="hidden" name="more_assistance10" value="No"/>
                        <input type="checkbox" name="more_assistance10"
                               value="Yes" <? echo strcasecmp ( $form[ 'more_assistance10' ] , "Yes" ) == 0 ? 'checked' : ''; ?>/>Other
                        (explain)</label><input
                        type="text" name="explain_box" size="25" maxlength="255"
                        value="<? echo $form[ 'explain_box' ] ?>"/></td>
            </tr>
        </table>


    </div>

    <div class="sixteen columns">
        <label><strong>Looking back, what advice would you give a freshman or sophomore student?</strong></label>
        <input type="text" name="advice" size="60" value="<? echo $form[ 'advice' ] ?>"/>
        <a class="button medium green" id="submit" onclick="checkFormFields()">Update Form</a>
        <a class="button medium red" href="/panel?action=deleteForm&formType=exitForm&id=<? echo $form[ 'order' ] ?>">Delete
            Form</a>

    </div>

    </div>
    <input type="hidden" name="location" value="<? echo $Bearden->getBeardenUser ()->getLocation (); ?>"/>
    <input type="hidden" name="entryFormID" value="<? echo $form[ 'order' ] ?>"/>
    <input type="hidden" name="action" value="updateExitForm"/>
    </form>


    <div id="footer">

        <!-- 960 Container -->
        <div class="container">

            <!-- About -->


            <!-- Twitter -->


        </div>
        <!-- 960 Container End -->

    </div>
    <!-- Footer End -->

    <!--  Footer Copyright-->
    <div id="footer-bottom">

        <!-- 960 Container -->
        <div class="container">

            <div class="eight columns">
                <div id="copyright">© Copyright 2012 <span>Bearden Guidance</span>. All Rights Reserved.</div>
            </div>

            <div class="eight columns">
                <ul class="social-links">
                    <li class="twitter"><a href="#">Twitter</a></li>
                    <li class="facebook"><a href="#">Facebook</a></li>
                    <li class="digg"><a href="#">Digg</a></li>
                    <li class="vimeo"><a href="#">Vimeo</a></li>
                    <li class="youtube"><a href="#">YouTube</a></li>
                    <li class="skype"><a href="#">Skype</a></li>
                </ul>
            </div>

        </div>
        <!-- End 960 Container -->

    </div>
    <!--  Footer Copyright End -->

    <!-- Back To Top Button -->
    <div id="backtotop"><a href="#"></a></div>

    <!-- Imagebox Build -->
    <script src="js/imagebox.build.js"></script>

    </body>
    </html>

<? } ?>