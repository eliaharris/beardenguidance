<?
/*  Include the Bearden file */

include_once "BeardenGuidance.php";

/* Create a Bearden Object */

$Bearden = new BeardenGuidance();

/* Check if the user logged in */

if ( !$Bearden->getBeardenUser ()->getIsLoggedIn () ) {

    /* They are not logged in, redirect and notify the user */

    $Bearden->getBeardenRedirect ()->redirectUser ( Redirects::PLEASE_LOGIN_REDIRECT , "Please login before continuing!" );

}


/* Check if the requested action is null  */

$Bearden->getBeardenActionHandler ()->getRequestedAction () !== NULL

    /* Execute the requested action */

    ? $Bearden->getBeardenActionHandler ()->executeRequestedAction ( $Bearden->getBeardenActionHandler ()->getRequestedAction () )

    /* There was no action requested, do nothing */

    : FALSE;


?>

<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html class="ie ie6" lang="en"><![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7" lang="en"><![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"><!--<![endif]-->
<head>

    <!-- Basic Page Needs
     ================================================== -->
    <meta charset="utf-8">
    <title>Bearden - Forum Results</title>

    <!-- Mobile Specific
     ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
     ================================================== -->
    <link rel="stylesheet" type="text/css" href="../css/style.css">

    <!-- Java Script
     ================================================== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script src="../js/flexslider.js"></script>
    <script src="../js/jquery.isotope.min.js"></script>
    <script src="../js/custom.js"></script>
    <script src="../js/ender.min.js"></script>
    <script src="../js/selectnav.js"></script>
    <script src="../js/imagebox.min.js"></script>
    <script src="../js/carousel.js"></script>
    <script src="../js/twitter.js"></script>
    <script src="../js/tooltip.js"></script>
    <script src="../js/popover.js"></script>
    <script src="../js/effects.js"></script>
    <style type="text/css">

        #results td {

            text-align: center;

        }

        #results th {

            text-align: center;
        }

    </style>
</head>
<body>

<!-- Header -->
<div id="header">

    <!-- 960 Container Start -->
    <div class="container ie-dropdown-fix">

        <!-- Logo -->
        <div class="four columns">
            <a href="#"><img src="../images/logo.png" alt=""/></a>
        </div>

        <!-- Main Navigation Start -->
        <div class="twelve columns">
            <div id="navigation">
                <ul id="nav">

                    <li><a id="current"
                           href="<? echo "?location=" . $Bearden->getBeardenUser ()->getLocation () . "&action=logout" ?>">Logout</a>
                    </li>

                </ul>
            </div>
        </div>
        <!-- Main Navigation End -->

    </div>
    <!-- 960 Container End -->

</div>
<!-- End Header -->

<div id="page-title">

    <!-- 960 Container Start -->
    <div class="container">
        <div class="sixteen columns">
            <h2>Form Results</h2>

        </div>
    </div>
    <!-- 960 Container End -->

</div>
<!-- 960 Container Start -->
<div class="container">
    <div class="three columns">
        <div class="sidebar">
            <!-- Search -->
            <div class="widget">
                <ul class="categories">
                    <li><a href="/panel.php?location=formResults">Form Results</a></li>
                    <li><a href="/panel.php?location=formResultsArchives">Form Results Archives</a></li>
                    <li><a href="/panel.php?location=feedback">Counselor Feedback</a></li>
                    <li><a href="/panel.php?location=account">Account Settings</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>
            </div>
        </div>
    </div>
    <br/>

    <div class="eleven columns">
        <?

        /*  Check if the message is empty */

        if ( !empty( $_GET[ 'message' ] ) ) {

            /*  Check if the message is GREEN */

            echo isset( $_GET[ 'green' ] )

                /* The message is green, make the HTML markup according  */

                ? '<div class="notification success closeable">'

                /* The message is green, make the HTML markup according  */

                : '<div class="notification error closeable">';

            /*  Now echo the proper message */

            echo '<p><span>' . $_GET[ 'message' ] . '</span></p>';

            /*  End the div for the message box */

            echo '</div>';
        }

        ?>


        <?

        /* Check to see if the user has selected the form type they want to view */

        if ( !( strcmp ( $_GET[ 'getForms' ] , "entryForms" ) == 0 || strcmp ( $_GET[ 'getForms' ] , "exitForms" ) == 0 ) ) {

            ?>

            <div class="sixteen columns">
                <h4 class="headline">Choose form type</h4>

                <table class="standard-table" id="results">

                    <tr>
                        <td>
                            <a class="small yellow button"
                               href="/panel?getForms=entryForms">Entry Forms</a>
                        </td>
                        <td>
                            <a class="small yellow button"
                               href="/panel?getForms=exitForms">Exit Forms</a>
                        </td>
                    </tr>

                </table>


            </div>

        <? } ?>



        <? if ( strcmp ( $_GET[ 'formType' ] , "entryForms" ) == 0 ) { ?>

            <div class="sixteen columns">

                <h4 class="headline">Entry Form Results</h4>

                <a href="/panel?getForms=entryForms&year=current">This Year</a>

                <a href="/panel?getForms=entryForms&year=all">All</a>


                <form id="attackForm" action="/panel" method="post"
                      enctype="multipart/form-data">

                    <table class="standard-table" id="results">

                        <tr>
                            <th>First</th>
                            <th>Last</th>
                            <th>Student ID</th>
                            <th>Date</th>
                            <th></th>

                        </tr>
                        <?

                        /* Execute the method to get the uploads in an array */

                        $formEntries = $Bearden->getBeardenActionHandler ()->getBeardenActions ()->getEntryForms ();

                        /* Go through all the uploads and print them */

                        foreach ( $formEntries as $form ) {

                            ?>

                            <tr>
                                <td><? echo $form[ 'firstname' ] ?> </td>
                                <td><? echo $form[ 'lastname' ] ?> </td>
                                <td><? echo $form[ 'studentID' ] ?> </td>
                                <td><? echo $form[ 'date' ] ?> </td>
                                <td><a class="small blue button"
                                       href="/panel?location=viewEntryFormResults&id=<? echo $form[ 'order' ] ?>">View
                                        Results</a>

                                </td>
                            </tr>

                        <? } ?>
                    </table>
                </form>
            </div>
        <? } ?>


        <? if ( strcmp ( $_GET[ 'formType' ] , "exitForms" ) == 0 ) { ?>

            <div class="sixteen columns">
                <h4 class="headline">Exit Form Results</h4>

                <a href="/panel?getForms=exitForms&year=current">This Year</a>

                <a href="/panel?getForms=exitForms&year=all">All</a>

                <form id="attackForm" action="/panel" method="post"
                      enctype="multipart/form-data">

                    <table class="standard-table" id="results">
                        <tr>
                            <th>First</th>
                            <th>Last</th>
                            <th>Student ID</th>
                            <th>Date</th>
                            <th></th>

                            <?

                            /* Execute the method to get the uploads in an array */

                            $formEntries = $Bearden->getBeardenActionHandler ()->getBeardenActions ()->getExitForms ();

                            /* Go through all the uploads and print them */

                            foreach ($formEntries as $form) {

                            ?>

                        <tr>
                            <td><? echo $form[ 'firstname' ] ?> </td>
                            <td><? echo $form[ 'lastname' ] ?> </td>
                            <td><? echo $form[ 'studentID' ] ?> </td>
                            <td><? echo $form[ 'date' ] ?> </td>
                            <td><a class="small blue button"
                                   href="/panel?location=viewExitFormResults&id=<? echo $form[ 'order' ] ?>">View
                                    Results</a></td>


                        </tr>

                        <? } ?>
                    </table>
                </form>
            </div>
        <? } ?>
    </div>
</div>

<div id="footer">

    <!-- 960 Container -->
    <div class="container">

    </div>
    <!-- 960 Container End -->

</div>
<!-- Footer End -->

<!--  Footer Copyright-->
<div id="footer-bottom">

    <!-- 960 Container -->
    <div class="container">

        <div class="eight columns">
            <div id="copyright">© Copyright 2014 <span>Bearden</span>. All Rights Reserved.</div>
        </div>

        <div class="eight columns">
            <ul class="social-links">
                <li class="twitter"><a href="#">Twitter</a></li>
                <li class="facebook"><a href="#">Facebook</a></li>
                <li class="digg"><a href="#">Digg</a></li>
                <li class="vimeo"><a href="#">Vimeo</a></li>
                <li class="youtube"><a href="#">YouTube</a></li>
                <li class="skype"><a href="#">Skype</a></li>
            </ul>
        </div>

    </div>
    <!-- End 960 Container -->

</div>
<!--  Footer Copyright End -->

<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>

<!-- Imagebox Build -->
<script src="../js/imagebox.build.js"></script>

</body>
</html>