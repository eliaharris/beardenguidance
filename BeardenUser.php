<?php
/**
 * Created by JetBrains PhpStorm.
 * User: elialanharris
 * Date: 7/24/13
 * Time: 2:06 PM
 * To change this template use File | Settings | File Templates.
 */

/*  Include the Bearden Guidance file */

include_once "BeardenGuidance.php";

class BeardenUser extends BeardenGuidance
{

    /**
     * @var
     * Variable that holds on the value of the username of the user
     */

    private $username;
    /**
     * @var
     * Variable that holds the value of the current website location the user is at
     */

    private $Location;
    /**
     * @var
     * Boolean if the user is logged in
     */

    private $isLoggedIn;
    /**
     * @var
     * Boolean if the user is taking the form
     */

    private $isTakingForm;
    /**
     * @var
     * The student ID of the user
     */

    private $studentID;
    /**
     * @var
     * The type of form the user is taking
     */

    private $formType;

    /**
     *
     */
    function __construct ()
    {

        if ( $this->getUserInformation () !== NULL ) {

            if ( json_decode ( $_SESSION[ 'user_data' ] )->studentID == NULL ) {

                /* Set the value of the username */

                $this->username = json_decode ( $_SESSION[ 'user_data' ] )->Username;

                /* Set the location the user is at */

                $this->Location = $_REQUEST[ 'location' ];

                /* Set if the user is logged in */

                $this->isLoggedIn = json_decode ( $_SESSION[ 'user_data' ] )->Status;

            } else {

                /* Set the student ID of the user taking the form*/

                $this->studentID = json_decode ( $_SESSION[ 'user_data' ] )->studentID;

                /* Set that the user is taking the form */

                $this->isTakingForm = TRUE;

                /* Set the form type the user is taking */

                $this->formType = json_decode ( $_SESSION[ 'user_data' ] )->formType;

                /* Set the location of the user */

                $this->Location = json_decode ( $_SESSION[ 'user_data' ] )->formType;

            }
        }
    }

    /**
     * @return JSON
     * Returns the session data of the user
     */

    public function getUserInformation ()
    {

        /* Return the user_data variable, its encoded to JSON */

        return $_SESSION[ 'user_data' ];

    }

    /**
     * @return mixed
     * Get's the form type of the user
     */
    public function getFormType ()
    {
        /*  Gets the form type the user is taking */

        return $this->formType;
    }

    /**
     * @return mixed
     * Gets if the user is taking the form
     */
    public function getIsTakingForm ()
    {
        /*  Return if the user is taking the form*/

        return $this->isTakingForm;
    }

    /**
     * @return mixed
     * Gets the student ID of the user that is taking the form
     */
    public function getStudentID ()
    {
        /*  Returns the user's student ID */

        return $this->studentID;
    }

    /**
     * @return boolean
     * Checks to see if the user is logged in
     */

    public
    function getIsLoggedIn ()
    {
        /* Check the variable isLoggedIn is equal to Logged_In */

        if ( strcmp ( $this->isLoggedIn , "Logged_In" ) == 0 ) {

            /* The user is logged in */

            return TRUE;

        } else {

            /* The user is not logged in */

            return FALSE;
        }
    }

    /**
     * @return mixed
     * Gets the value of the variable Location
     */

    public
    function getLocation ()
    {
        /* Return the location the user is at */

        return $this->Location;
    }

    /**
     * @param mixed $Location
     * Set the location the user is at
     */
    public function setLocation ( $Location )
    {
        /* Set's the location of the user */

        $this->Location = $Location;
    }

    /**
     * @return string
     * Gets the value of the variable Username
     */

    public
    function getUsername ()
    {
        /* Return the username of the user */

        return $this->username;
    }

    /**
     * @param $JSON
     * Sets the user's information in the session variable
     */
    public function setUserInformation ( $JSON )
    {

        /* Sets the user information (login, member, etc) */

        $_SESSION[ 'user_data' ] = $JSON;

    }

}