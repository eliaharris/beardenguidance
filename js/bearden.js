function checkFormFields() {

    /* Get the input elements */

    var requiredElements = new Array('firstname', 'lastname', 'address', 'city', 'zipcode', 'email', 'studentcell', 'meetwith');

    /*  Counter tha is used in the for loop */

    var i;

    /* The string that holds all the fields that need to be filled in */

    var requiredFieldsString = '';

    for (i = 0; i < requiredElements.length; i++) {

        /* Get the current required element */

        var currentRequiredElement = document.getElementsByName(requiredElements[i]).item(0);

        /* Add the value to the string */

        switch (requiredElements[i]) {

            case 'firstname':

                currentRequiredElement.value == ''

                    ? requiredFieldsString = requiredFieldsString + 'Please fill in your first name!\n'

                    : false;


                break;
            case 'lastname':

                currentRequiredElement.value == ''

                    ? requiredFieldsString = requiredFieldsString + 'Please fill in your last name!\n'

                    : false;


                break;

            case 'address':

                currentRequiredElement.value == ''

                    ? requiredFieldsString = requiredFieldsString + 'Please fill in your address!\n'

                    : false;


                break;

            case 'city':

                currentRequiredElement.value == ''

                    ? requiredFieldsString = requiredFieldsString + 'Please fill in your city!\n'

                    : false;


                break;

            case 'zipcode':

                currentRequiredElement.value == ''

                    ? requiredFieldsString = requiredFieldsString + 'Please fill in your zipcode!\n'

                    : false;


                break;

            case 'email':

                currentRequiredElement.value == ''

                    ? requiredFieldsString = requiredFieldsString + 'Please fill in your email!\n'

                    : false;


                break;

            case 'studentcell':

                currentRequiredElement.value == ''

                    ? requiredFieldsString = requiredFieldsString + 'Please fill in your cellphone number!\n'

                    : false;

                break;


            default:

                break;


        }

    }


    /* Check if the string is still empty */

    if (requiredFieldsString !== '') {

        /* It's not empty, they were missing fields */

        alert(requiredFieldsString);

    } else {

        /* Everything was filled in, we can now submit the form */

        document.getElementById("formSubmit").submit();


    }


}
