<?php
/**
 * Created by JetBrains PhpStorm.
 * User: elialanharris
 * Date: 7/30/13
 * Time: 3:45 PM
 * To change this template use File | Settings | File Templates.
 */

class BeardenActions extends BeardenActionHandler
{

    /**
     * We do nothing with the construct method
     */

    function __construct ()
    {

        /* Do nothing here */

    }

    /**
     * The student wants to take a form. Verify data and give them the form
     */
    public function takeFormAction ()
    {

        /* Make an array to hold the data */

        $takeFormData = array();

        /* Add the form type into the array */

        $takeFormData[ 'formType' ] = $_POST[ 'formType' ];

        /*  Add the student ID into the array */

        $takeFormData[ 'studentID' ] = $_POST[ 'studentID' ];

        /*  Now check the data that was passed*/

        $takeFormData = $this->getBeardenCheckData ()->checkData ( $takeFormData , CheckData::TAKE_FORM_DATA );

        /* Now set the user information to the form data passed */

        /* Check if the student ID already exists */

        $response = $this->getBeardenMySQL ()->executeQuery ( Query::CHECK_STUDENT_ID , $takeFormData );

        /* Turn the array into JSON */

        $JSONResponse = json_decode ( $response );

        /* Check if there was an error */


        $JSONResponse->error

            /* There was an error, redirect and notify the user */

            ? $this->getBeardenRedirect ()->redirectUser ( Redirects::TAKE_FORM_FAIL_REDIRECT , "An error occurred! Please try again!" )

            /* There wasn't an error, we can continue */

            : FALSE;


        /* Check if the student ID already exists */

        $JSONResponse->StudentIDExists

            /* The student ID already exists, redirect and notify the user */

            ? $this->getBeardenRedirect ()->redirectUser ( Redirects::TAKE_FORM_FAIL_REDIRECT , "This student ID has already been used for this form! Please contact the counseling office." )

            /* The student ID does not exist, we can continue to take them to the form */

            : FALSE;

        $this->getBeardenUser ()->setUserInformation ( json_encode ( $takeFormData ) );

        /* Set the location of the user */

        $this->getBeardenUser ()->setLocation ( $takeFormData[ 'formType' ] );

        /*  Redirect to the right form page */

        $this->getBeardenRedirect ()->redirectUser ( Redirects::GREEN_REDIRECT , "You are now taking the form! Please fill in all the fields that have asterisks." );


    }

    /**
     * Action for updating a specific entry form
     */
    public function updateEntryFormAction ()
    {

        /* The final query that will be submitted */

        $finalQuery = NULL;

        /* Holds the fields that are being updated */

        $updateFieldsString = NULL;

        /* Array that holds the keys */

        $fieldsToInsertKeyArray = array();

        /* Array that holds the values of the keys  */

        $fieldsToInsertValuesArray = array();

        /*  The entry form ID that we will be updating */

        $entryFormIDToUpdate = NULL;

        /* Clean the values passed though the post variable to prepare them for the query*/

        $cleanedPostData = array_map ( 'mysql_real_escape_string' , $_POST );

        /* Use the foreach to get the field names and the values */

        foreach ( $cleanedPostData as $Key => $KeyValue ) {

            /* Skip over when we hit the location key and action key */

            if ( strcasecmp ( $Key , "location" ) == 0 || strcasecmp ( $Key , "action" ) == 0 )
                continue;
            if ( strcasecmp ( $Key , "entryFormID" ) == 0 ) {

                /* Now save the entry form ID so we know which one we are updating*/

                $entryFormIDToUpdate = $KeyValue;

                /* Continue as we don't want to add it into the main array*/

                continue;
            }

            /* Add the key into the array */

            $fieldsToInsertKeyArray[ ] = $Key;

            /* Add the value of the key into the array */

            $fieldsToInsertValuesArray[ ] = $KeyValue;

        }


        /* Use the for loop to go through each index until we reach the last index of array */

        for ( $indexCount = 0 ; $indexCount <= count ( $fieldsToInsertKeyArray ) ; $indexCount++ ) {

            /* Check if the index count is still less than the array count */

            if ( $indexCount < count ( $fieldsToInsertKeyArray ) - 1 ) {

                /* Append the field name on the string and we add a comma */

                $updateFieldsString .= "`" . $fieldsToInsertKeyArray [ $indexCount ] . "` = '" . $fieldsToInsertValuesArray [ $indexCount ] . "' ,";


            } else {

                /* When we reach here, we are at the last index */

                /* Append the field name on the string and we add a comma */

                $updateFieldsString .= "`" . $fieldsToInsertKeyArray [ $indexCount ] . "` = '" . $fieldsToInsertValuesArray [ $indexCount ] . "'";


                break;

            }
        }
        /* The final query that will be submitted */

        $finalQuery = "UPDATE entryForm SET " . $updateFieldsString . " WHERE `order` = '" . $entryFormIDToUpdate . "'";

        /* Execute the final query */


        $response = $this->getBeardenMySQL ()->manualQuery ( $finalQuery );

        if ( $response ) {

            /* The query succeeded. Redirect and notify the user */

            $this->getBeardenRedirect ()->redirectUser ( Redirects::FORM_RESULTS_GREEN_REDIRECT , "The form was successfully updated!" );


        } else {

            /* The query succeeded. Redirect and notify the user */

            $this->getBeardenRedirect ()->redirectUser ( Redirects::FORM_RESULTS_RED_REDIRECT , "Something went wrong, please try again!" );

        }

    }

    /**
     * Action for deleting a form from the database
     */
    public function deleteForm ()
    {

        /* Make an array for the data that will be put in */

        $deleteFormArray = array();

        /* Add the form type into the array */

        $deleteFormArray[ 'formType' ] = $_GET[ 'formType' ];

        /* Add the form ID that we are deleting */

        $deleteFormArray[ 'formID' ] = $_GET[ 'id' ];

        /* Clean the data with the mysql_real_escape string, prepare it to be passed in a query */

        $deleteFormArray = array_map ( 'mysql_real_escape_string' , $deleteFormArray );

        /* Clean the data with the filter_var, remove any html that could be in the data */

        $deleteFormArray = array_map ( 'filter_var' , $deleteFormArray );

        /* Execute the delete form query and get the response */

        $response = $this->getBeardenMySQL ()->executeQuery ( Query::DELETE_FORM , $deleteFormArray );

        /* Turn the array into JSON */

        $JSONResponse = json_decode ( $response );

        /* Check if there was an error */

        $JSONResponse->error

            /* There was an error, redirect and notify the user */

            ? $this->getBeardenRedirect ()->redirectUser ( Redirects::FORM_RESULTS_RED_REDIRECT , "An error occurred! Please try again!" )

            /* There wasn't an error, we can continue */

            : FALSE;


        /* Check if the form successfully deleted */

        $JSONResponse->didDelete

            /* The form did delete, redirect and notify the user */

            ? $this->getBeardenRedirect ()->redirectUser ( Redirects::FORM_RESULTS_GREEN_REDIRECT , "The form was deleted! The student can now take the form again." )

            /* The form did not delete, redirect and notify the user */

            : $this->getBeardenRedirect ()->redirectUser ( Redirects::FORM_RESULTS_RED_REDIRECT , "The form did not delete! Please try again." );

    }

    /**
     * Action for updating a specific entry form
     */
    public function updateExitFormAction ()
    {

        /* The final query that will be submitted */

        $finalQuery = NULL;

        /* Holds the fields that are being updated */

        $updateFieldsString = NULL;

        /* Array that holds the keys */

        $fieldsToInsertKeyArray = array();

        /* Array that holds the values of the keys  */

        $fieldsToInsertValuesArray = array();

        /*  The entry form ID that we will be updating */

        $entryFormIDToUpdate = NULL;

        /* Clean the values passed though the post variable to prepare them for the query*/

        $cleanedPostData = array_map ( 'mysql_real_escape_string' , $_POST );

        /* Clean the data with the filter_var, remove any html that could be in the data */

        $cleanedPostData = array_map ( 'filter_var' , $cleanedPostData );

        /* Use the foreach to get the field names and the values */

        foreach ( $cleanedPostData as $Key => $KeyValue ) {

            /* Skip over when we hit the location key and action key */

            if ( strcasecmp ( $Key , "location" ) == 0 || strcasecmp ( $Key , "action" ) == 0 )
                continue;
            if ( strcasecmp ( $Key , "entryFormID" ) == 0 ) {

                /* Now save the entry form ID so we know which one we are updating*/

                $entryFormIDToUpdate = $KeyValue;

                /* Continue as we don't want to add it into the main array*/

                continue;
            }

            /* Add the key into the array */

            $fieldsToInsertKeyArray[ ] = $Key;

            /* Add the value of the key into the array */

            $fieldsToInsertValuesArray[ ] = $KeyValue;

        }


        /* Use the for loop to go through each index until we reach the last index of array */

        for ( $indexCount = 0 ; $indexCount <= count ( $fieldsToInsertKeyArray ) ; $indexCount++ ) {

            /* Check if the index count is still less than the array count */

            if ( $indexCount < count ( $fieldsToInsertKeyArray ) - 1 ) {

                /* Append the field name on the string and we add a comma */

                $updateFieldsString .= "`" . $fieldsToInsertKeyArray [ $indexCount ] . "` = '" . $fieldsToInsertValuesArray [ $indexCount ] . "' ,";


            } else {

                /* When we reach here, we are at the last index */

                /* Append the field name on the string and we add a comma */

                $updateFieldsString .= "`" . $fieldsToInsertKeyArray [ $indexCount ] . "` = '" . $fieldsToInsertValuesArray [ $indexCount ] . "'";


                break;

            }
        }

        /* The final query that will be submitted */

        $finalQuery = "UPDATE exitForm SET " . $updateFieldsString . " WHERE `order` = '" . $entryFormIDToUpdate . "'";


        /* Execute the final query */


        $response = $this->getBeardenMySQL ()->manualQuery ( $finalQuery );

        if ( $response ) {

            /* The query succeeded. Redirect and notify the user */

            $this->getBeardenRedirect ()->redirectUser ( Redirects::FORM_RESULTS_GREEN_REDIRECT , "The form was successfully updated!" );


        } else {

            /* The query succeeded. Redirect and notify the user */

            $this->getBeardenRedirect ()->redirectUser ( Redirects::FORM_RESULTS_RED_REDIRECT , "Something went wrong, please try again!" );

        }

    }

    /**
     * Action for cancelling a form
     */
    public function cancelFormAction ()
    {


        /* Start a new session */

        session_start ();

        /* Destroy the session */

        session_destroy ();

        $this->getBeardenRedirect ()->redirectUser ( Redirects::TAKE_FORM_FAIL_REDIRECT , "You successfully cancelled the form!" );


    }

    /**
     * @return mixed
     * Action that get all the entry forms
     */
    public function getEntryForms ()
    {

        /* Execute the query and get the response */

        $response = $this->getBeardenMySQL ()->executeQuery ( Query::GET_ENTRY_FORMS_QUERY , NULL );

        /* Decode the JSON¬ */

        $responseJSON = json_decode ( $response );

        /* Now return the data array */

        $linksArray = json_encode ( $responseJSON->dataArray );

        /* Now we return the JSON as an array */

        return json_decode ( $linksArray , TRUE );

    }

    /**
     * @return mixed
     * Action that gets all the exit forms
     */
    public function getExitForms ()
    {

        /* Execute the query and get the response */

        $response = $this->getBeardenMySQL ()->executeQuery ( Query::GET_EXIT_FORMS_QUERY , NULL );

        /* Decode the JSON¬ */

        $responseJSON = json_decode ( $response );

        /* Now return the data array */

        $linksArray = json_encode ( $responseJSON->dataArray );

        /* Now we return the JSON as an array */

        return json_decode ( $linksArray , TRUE );

    }

    /**
     * @return mixed
     * Action for getting a specific entry form
     */
    public function getEntryForm ()
    {

        /*  Make an array that will hold the data that is passed */

        $entryFormArray = array();

        /*  Add the ID into the array */

        $entryFormArray[ 'entryFormID' ] = mysql_real_escape_string ( $_GET[ 'id' ] );

        /* Execute the query and get the results */

        $response = $this->getBeardenMySQL ()->executeQuery ( Query::GET_ENTRY_FORM , $entryFormArray );

        /* Decode the JSON¬ */

        $responseJSON = json_decode ( $response );

        /* Now return the data array */

        $resultsArray = json_encode ( $responseJSON->dataArray );

        /* Now we return the JSON as an array */

        return json_decode ( $resultsArray , TRUE );


    }

    /**
     * @return mixed
     * Action for getting a specific exit form
     */
    public function getExitForm ()
    {

        /*  Make an array that will hold the data that is passed */

        $exitFormArray = array();

        /*  Add the ID into the array */

        $exitFormArray[ 'exitFormID' ] = mysql_real_escape_string ( $_GET[ 'id' ] );

        /* Execute the query and get the results */

        $response = $this->getBeardenMySQL ()->executeQuery ( Query::GET_EXIT_FORM , $exitFormArray );

        /* Decode the JSON¬ */

        $responseJSON = json_decode ( $response );

        /* Now return the data array */

        $resultsArray = json_encode ( $responseJSON->dataArray );

        /* Now we return the JSON as an array */

        return json_decode ( $resultsArray , TRUE );


    }

    /**
     * Submits the user's form to the database
     */

    public function entryFormSubmitAction ()
    {


        /* The final query that will be submitted */

        $finalQuery = NULL;

        /* Fields that we will insert the data to */

        $fieldsToInsertString = NULL;

        /* Values that will be inserted for the fields*/

        $valuesForFieldsString = NULL;

        /* Array that holds the keys */

        $fieldsToInsertKeyArray = array();

        /* Array that holds the values of the keys  */

        $fieldsToInsertValuesArray = array();

        /* Clean the values passed though the post variable to prepare them for the query*/

        $cleanedPostData = array_map ( 'mysql_real_escape_string' , $_POST );

        /* Use the foreach to get the field names and the values */

        foreach ( $cleanedPostData as $Key => $KeyValue ) {

            /* Skip over when we hit the location key and action key */

            if ( strcasecmp ( $Key , "location" ) == 0 || strcasecmp ( $Key , "action" ) == 0 )
                continue;
            /* Add the key into the array */

            $fieldsToInsertKeyArray[ ] = $Key;

            /* Add the value of the key into the array */

            $fieldsToInsertValuesArray[ ] = $KeyValue;

        }


        /* Use the for loop to go through each index until we reach the last index of array */

        for ( $indexCount = 0 ; $indexCount <= count ( $fieldsToInsertKeyArray ) ; $indexCount++ ) {

            /* Check if the index count is still less than the array count */

            if ( $indexCount < count ( $fieldsToInsertKeyArray ) - 1 ) {

                /* Append the field name on the string and we add a comma */

                $fieldsToInsertString .= "`" . $fieldsToInsertKeyArray [ $indexCount ] . '`,';

                /* Append the value of the key to the string */

                $valuesForFieldsString .= "'" . $fieldsToInsertValuesArray [ $indexCount ] . "',";

            } else {

                /* When we reach here, we are at the last index */

                /* Append the field name on the string without the comma */

                $fieldsToInsertString .= "`" . $fieldsToInsertKeyArray[ $indexCount ] . '`';

                /* Append the value of the key to the string */

                $valuesForFieldsString .= "'" . $fieldsToInsertValuesArray [ $indexCount ] . "'";

                break;

            }
        }
        /* The final query that will be submitted */

        $finalQuery = "INSERT INTO entryForm (" . $fieldsToInsertString . ") VALUES (" . $valuesForFieldsString . ")";

        /* Execute the final query */

        $response = $this->getBeardenMySQL ()->manualQuery ( $finalQuery );

        if ( $response ) {

            /* The query succeeded. Redirect and notify the user */

            $this->getBeardenRedirect ()->redirectUser ( Redirects::TAKE_FORM_SUCCESS_REDIRECT , "Your form has been submitted!" );


        } else {

            /* The query went wrong. Redirect and notify the user */

            $this->getBeardenRedirect ()->redirectUser ( Redirects::TAKE_FORM_FAIL_REDIRECT , "Something went wrong.. Please try again later or contact the counselor's office!" );
        }


        exit;

    }

    /**
     * Submits the user's form to the database
     */

    public function exitFormSubmitAction ()
    {


        /* The final query that will be submitted */

        $finalQuery = NULL;

        /* Fields that we will insert the data to */

        $fieldsToInsertString = NULL;

        /* Values that will be inserted for the fields*/

        $valuesForFieldsString = NULL;

        /* Array that holds the keys */

        $fieldsToInsertKeyArray = array();

        /* Array that holds the values of the keys  */

        $fieldsToInsertValuesArray = array();

        /* Clean the values passed though the post variable to prepare them for the query*/

        $cleanedPostData = array_map ( 'mysql_real_escape_string' , $_POST );

        /* Use the foreach to get the field names and the values */

        foreach ( $cleanedPostData as $Key => $KeyValue ) {

            /* Skip over when we hit the location key and action key */

            if ( strcasecmp ( $Key , "location" ) == 0 || strcasecmp ( $Key , "action" ) == 0 )
                continue;
            /* Add the key into the array */

            $fieldsToInsertKeyArray[ ] = $Key;

            /* Add the value of the key into the array */

            $fieldsToInsertValuesArray[ ] = $KeyValue;

        }


        /* Use the for loop to go through each index until we reach the last index of array */

        for ( $indexCount = 0 ; $indexCount <= count ( $fieldsToInsertKeyArray ) ; $indexCount++ ) {

            /* Check if the index count is still less than the array count */

            if ( $indexCount < count ( $fieldsToInsertKeyArray ) - 1 ) {

                /* Append the field name on the string and we add a comma */

                $fieldsToInsertString .= "`" . $fieldsToInsertKeyArray [ $indexCount ] . '`,';

                /* Append the value of the key to the string */

                $valuesForFieldsString .= "'" . $fieldsToInsertValuesArray [ $indexCount ] . "',";

            } else {

                /* When we reach here, we are at the last index */

                /* Append the field name on the string without the comma */

                $fieldsToInsertString .= "`" . $fieldsToInsertKeyArray[ $indexCount ] . '`';

                /* Append the value of the key to the string */

                $valuesForFieldsString .= "'" . $fieldsToInsertValuesArray [ $indexCount ] . "'";

                break;

            }
        }
        /* The final query that will be submitted */

        $finalQuery = "INSERT INTO exitForm (" . $fieldsToInsertString . ") VALUES (" . $valuesForFieldsString . ")";

        /* Execute the final query */

        $response = $this->getBeardenMySQL ()->manualQuery ( $finalQuery );

        if ( $response ) {

            /* The query succeeded. Redirect and notify the user */

            $this->getBeardenRedirect ()->redirectUser ( Redirects::TAKE_FORM_SUCCESS_REDIRECT , "Your form has been submitted!" );


        } else {

            /* The query went wrong. Redirect and notify the user */

            $this->getBeardenRedirect ()->redirectUser ( Redirects::TAKE_FORM_FAIL_REDIRECT , "Something went wrong.. Please try again later or contact the counselor's office!" );
        }


        exit;

    }

    /**
     * Logs an administrator in
     */

    public
    function loginAction ()
    {

        /* Make the credentials array */

        $credentials = array();

        /* Add the username into the array */

        $credentials[ 'username' ] = $_POST[ 'username' ];

        /* Add the password into the array */

        $credentials[ 'password' ] = $_POST[ 'password' ];

        /* Now put the login data into the checkData() to clean and verify they
        are valid credentials that can be passed. It also cleans the variables to prepare them
        for MySQl queries */

        $credentials = $this->getBeardenCheckData ()->checkData ( $credentials , CheckData::LOGIN_DATA );

        /* Execute the login query and save its response */

        $response = $this->getBeardenMySQL ()->executeQuery ( BeardenMySQL::LOGIN_QUERY , $credentials );

        /* Decode the JSON array so we can check its returnArray values */

        $responseJSON = json_decode ( $response );

        /* Put the information into the the sessionData variable */

        $sessionData = json_encode ( $responseJSON->sessionData );

        /* Call the BeardenUser object's method to set the data into the session variable */

        $this->getBeardenUser ()->setUserInformation ( $sessionData );

        /* Checking if the user entered invalid credentials or if there was an error in the MySQL query */

        $responseJSON->invalidCredentials == TRUE || $responseJSON->error == TRUE

            /* Invalid credentials were entered, redirect the user and notify them */

            ? $this->getBeardenRedirect ()->redirectUser ( Redirects::LOGIN_FAIL_REDIRECT , "Invalid username or password!" )

            /* Valid credentials were entered, we can now continue */

            : FALSE;

        /* Checking for possible errors in the response of the MySQL Query */

        $responseJSON->error == TRUE

            /* A rare error occurred, redirect and notify the user */

            ? $this->getBeardenRedirect ()->redirectUser ( Redirects::LOGIN_FAIL_REDIRECT , "Something went wrong! Please try again!" )

            /* There was no error, and the user successfully logged in, redirect the user */

            : $this->getBeardenRedirect ()->redirectUser ( Redirects::LOGIN_SUCCESS_REDIRECT , NULL );

    }

    /**
     * Registers a new user
     */
    public
    function registerAction ()
    {

        /* Create an array for the register data that is passed */

        $registerInfo = array();

        /* Add the username passed to the array */

        $registerInfo[ 'username' ] = $_POST[ 'username' ];

        /* Add the password passed to the array */

        $registerInfo[ 'password' ] = $_POST[ 'password' ];

        /* Add the confirm password passed to the array */

        $registerInfo[ 'confirm' ] = $_POST[ 'confirm' ];

        /* Add the email passed to the array */

        $registerInfo[ 'email' ] = $_POST[ 'email' ];

        /* Add the referral's username passed to the array */

        $registerInfo[ 'refer' ] = $_POST[ 'refer' ];

        /* Now put the register data into the checkData() to clean and verify they
        are valid credentials that can be passed. It also cleans the variables to prepare them
        for MySQl queries */

        $registerInfo = $this->getBeardenCheckData ()->checkData ( $registerInfo , CheckData::REGISTER_DATA );

        /* Execute the register query and save its response */

        $response = $this->getBeardenMySQL ()->executeQuery ( BeardenMySQL::REGISTER_QUERY , $registerInfo );

        /* Decode the JSON array so we can check its returnArray values */

        $responseJSON = json_decode ( $response );

        /* Check the email and username has been taken */

        $responseJSON->emailTaken && $responseJSON->userTaken

            /* Both the username and email has been registered before, redirect and notify the user */

            ? $this->getBeardenRedirect ()->redirectUser ( Redirects::REGISTER_FAILED_REDIRECT , "The email and username have already been registered!" )

            /* Both as a pair were not taken, further checking will be done*/

            : FALSE;

        /* Check the email and see if it's been taken */

        $responseJSON->emailTaken

            /* The email has been registered before, redirect and notify the user */

            ? $this->getBeardenRedirect ()->redirectUser ( Redirects::REGISTER_FAILED_REDIRECT , "This email has already been registered with an account!" )

            /*The email has not been taken, continue to check username */

            : FALSE;

        /* Check the username and see if it's been taken */

        $responseJSON->userTaken

            /* The username has been registered before, redirect and notify the user */

            ? $this->getBeardenRedirect ()->redirectUser ( Redirects::REGISTER_FAILED_REDIRECT , "This username has already been registered with an account!" )

            /*The username has not been taken, continue to check username */

            : FALSE;

        /* Checking for possible errors in the response of the MySQL Query */

        $responseJSON->error == TRUE

            /* A rare error occurred, redirect and notify the user */

            ? $this->getBeardenRedirect ()->redirectUser ( Redirects::REGISTER_FAILED_REDIRECT , "Something went wrong! Please try again!" )

            /* There were no errors, redirect and notify the user their account has been registered */

            : $this->getBeardenRedirect ()->redirectUser ( Redirects::REGISTER_SUCCESS_REDIRECT , "You have successfully registered your account!&green" );


    }

    /**
     * Logs out the user
     */
    public
    function logoutAction ()
    {


        session_start ();

        /* Now destroy it */

        session_destroy ();

        /* Redirect the user and tell them they have successfully logged out */

        $this->getBeardenRedirect ()->redirectUser ( Redirects::LOGOUT_REDIRECT , "You have successfully logged out!" );

    }
}