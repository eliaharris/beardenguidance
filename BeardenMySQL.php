<?php
/**
 * Created by JetBrains PhpStorm.
 * User: elialanharris
 * Date: 7/12/13
 * Time: 11:39 AM
 * To change this template use File | Settings | File Templates.
 */
require_once "Constants.php";
//Require the constants


/**
 * Class BeardenMySQL
 */
class BeardenMySQL implements Query
{


    /**
     * @var
     * Holds the connection to the database
     */
    private $MYSQL_CONNECTION;

    /**
     *
     */
    function __destruct ()
    {


        $this->MYSQL_CONNECTION = null;
        //Null out the connection
    }

    /**
     * @param $QUERY
     * @param $parameters
     * @return null|string
     */
    public function executeQuery ( $QUERY , $parameters )
    {

        switch ( $QUERY ) {
            //Find the case that matches $QUERY

            case $this::LOGIN_QUERY:

                //Execute the query that requests if the specific username and password exist in the database!

                $this->connectToDatabase ();
                //Connect to the database

                $returnArray = array();
                //Array that will be returned

                $response = mysqli_query ( $this->MYSQL_CONNECTION , vsprintf ( $this::LOGIN_QUERY , $parameters ) );
                //return the data that was returned from the query

                $response->num_rows == 0 ? $returnArray[ 'invalidCredentials' ] = TRUE : TRUE;
                //Check and see if the response returned zero fields

                while ( $row = $response->fetch_assoc () ) {
                    //Parses the result into a row

                    if ( strcasecmp ( $row[ 'username' ] , $parameters[ 'username' ] ) == 0 && strcmp ( $row[ 'password' ] , $parameters[ 'password' ] ) == 0 ) {
                        //Recheck the credentials to the result to make sure they match

                        $returnArray[ 'sessionData' ] = array("Status" => "Logged_In" , "Username" => $row[ 'user' ]);
                        //Set the user data into a session array and decode it into JSON

                        break;
                        //Breaks from the while loop

                    } else {

                        $_SESSION[ 'invalidCredentials' ] = TRUE;
                        //Set the user data to null

                    }

                }

                if ( !$response ) {
                    //If the query failed

                    $returnArray[ 'error' ] = TRUE;
                    //Set that there was an error


                }

                mysqli_close ( $this->MYSQL_CONNECTION );
                //Close the connection to the database

                return json_encode ( $returnArray );
                //Return the response

                break;


            /* Query for getting the entry forms */

            case $this::GET_EXIT_FORMS_QUERY:

                /* Connect to the database */

                $this->connectToDatabase ();

                /* Array that will be returned */

                $returnArray = array();

                /* Execute the query and save the response */

                $response = mysqli_query ( $this->MYSQL_CONNECTION , $this::GET_EXIT_FORMS_QUERY );

                /* Check if the response was false */

                $response == FALSE

                    /* It was false, there was an error */

                    ? $returnArray[ 'error' ] = TRUE

                    /* Everything seemed to checkout, redirect and notify the user */

                    : FALSE;

                /* Create an array */

                $array = array();

                /* Fetch the array */

                while ( $row = mysqli_fetch_assoc ( $response ) ) {

                    $array[ ] = $row;
                }

                /*  Add the fetch array into the return array */

                $returnArray[ 'dataArray' ] = $array;

                /* Return the returnArray and encode it to JSON */

                return json_encode ( $returnArray );


                break;

            /* Query for getting the entry forms */

            case $this::GET_ENTRY_FORMS_QUERY:

                /* Connect to the database */

                $this->connectToDatabase ();

                /* Array that will be returned */

                $returnArray = array();

                /* Execute the query and save the response */


                $response = mysqli_query ( $this->MYSQL_CONNECTION , $this::GET_ENTRY_FORMS_QUERY );


                /* Check if the response was false */

                $response == FALSE

                    /* It was false, there was an error */

                    ? $returnArray[ 'error' ] = TRUE

                    /* Everything seemed to checkout, redirect and notify the user */

                    : FALSE;

                /* Create an array */

                $array = array();

                /* Fetch the array */

                while ( $row = mysqli_fetch_assoc ( $response ) ) {

                    $array[ ] = $row;
                }

                /*  Add the fetch array into the return array */

                $returnArray[ 'dataArray' ] = $array;

                /* Return the returnArray and encode it to JSON */

                return json_encode ( $returnArray );


                break;

            /*  Getting the results of an entry form */

            case $this::GET_ENTRY_FORM:


                /* Connect to the database */

                $this->connectToDatabase ();

                /* Array that will be returned */

                $returnArray = array();

                /* Execute the query and save the response */

                $response = mysqli_query ( $this->MYSQL_CONNECTION , vsprintf ( $this::GET_ENTRY_FORM , $parameters ) );

                /* Check if the response was false */

                $response == FALSE

                    /* It was false, there was an error */

                    ? $returnArray[ 'error' ] = TRUE

                    /* Everything seemed to checkout, redirect and notify the user */

                    : FALSE;

                /* Create an array */

                $array = array();

                /* Fetch the array */

                while ( $row = mysqli_fetch_assoc ( $response ) ) {

                    $array[ ] = $row;
                }

                /*  Add the fetch array into the return array */

                $returnArray[ 'dataArray' ] = $array;

                /* Return the returnArray and encode it to JSON */

                return json_encode ( $returnArray );


                break;


            /*  Getting the results of an entry form */

            case $this::GET_EXIT_FORM:

                /* Connect to the database */

                $this->connectToDatabase ();

                /* Array that will be returned */

                $returnArray = array();

                /* Execute the query and save the response */

                $response = mysqli_query ( $this->MYSQL_CONNECTION , vsprintf ( $this::GET_EXIT_FORM , $parameters ) );

                /* Check if the response was false */

                $response == FALSE

                    /* It was false, there was an error */

                    ? $returnArray[ 'error' ] = TRUE

                    /* Everything seemed to checkout, redirect and notify the user */

                    : FALSE;

                /* Create an array */

                $array = array();

                /* Fetch the array */

                while ( $row = mysqli_fetch_assoc ( $response ) ) {

                    $array[ ] = $row;
                }

                /*  Add the fetch array into the return array */

                $returnArray[ 'dataArray' ] = $array;

                /* Return the returnArray and encode it to JSON */

                return json_encode ( $returnArray );


                break;

            /* Case for checking if a Student ID already exists within a certain form */

            case $this::CHECK_STUDENT_ID:


                /* Connect to the database */

                $this->connectToDatabase ();

                /* Array that will be returned */

                $returnArray = array();

                /* Execute the query and save the response */

                $response = mysqli_query ( $this->MYSQL_CONNECTION , vsprintf ( $this::CHECK_STUDENT_ID , $parameters ) );

                /* Check if the response was false */


                $response == FALSE

                    /* It was false, there was an error */

                    ? $returnArray[ 'error' ] = TRUE

                    /* Everything seemed to checkout, redirect and notify the user */

                    : FALSE;

                /* Check if there were one or more rows returned */

                mysqli_num_rows ( $response ) >= 0

                    /* There was more than one row returned */

                    ? $returnArray[ 'StudentIDExists' ] = TRUE

                    /* There wasn't any rows returned */

                    : $returnArray[ 'StudentIDExists' ] = FALSE;


                /* Return the returnArray and encode it to JSON */

                return json_encode ( $returnArray );

                break;

            /* Case for deleting a form from the database */

            case $this::DELETE_FORM:


                /* Connect to the database */

                $this->connectToDatabase ();


                /* Array that will be returned */

                $returnArray = array();

                /* Execute the query and save the response */

                $response = mysqli_query ( $this->MYSQL_CONNECTION , vsprintf ( $this::DELETE_FORM , $parameters ) );

                /* Check if the response was false */

                $response == FALSE

                    /* It was false, there was an error */

                    ? $returnArray[ 'error' ] = TRUE

                    /* Everything seemed to checkout, redirect and notify the user */

                    : FALSE;

                /* Check if was successfully deleted */

                mysqli_affected_rows ( $this->MYSQL_CONNECTION ) >= 0

                    /* It was deleted */

                    ? $returnArray[ 'didDelete' ] = TRUE

                    /* It was not deleted */

                    : $returnArray[ 'didDelete' ] = FALSE;

                /* Return the returnArray and encode it to JSON */

                return json_encode ( $returnArray );

                break;


            /* The default case, we should never reach here */

            default:


                break;
        }

        /* Return NULL if we ever get to this point */

        return NULL;
    }

    /**
     *
     */
    private function connectToDatabase ()
    {
        $this->MYSQL_CONNECTION = new mysqli( $this::MYSQL_SERVER , $this::MYSQL_USER , $this::MYSQL_PASS , $this::MYSQL_DATABASE );
        //Defined the connection information to the database

        $this->MYSQL_CONNECTION->connect_errno ? print( "There was a problem, please try again" ) : FALSE;
        //Check if there were any errors connecting to the database
    }

    public function manualQuery ( $query )
    {


        /* Connect to the database */

        $this->connectToDatabase ();

        /* Execute the query */

        $response = mysqli_query ( $this->MYSQL_CONNECTION , $query );


        return $response;
    }
}