<?php

/**
 * Created by JetBrains PhpStorm.
 * User: elialanharris
 * Date: 7/3/13
 * Time: 1:25 PM
 */


/* Require the Action Handler */


/**
 * Class Bearden
 */
class BeardenGuidance
{

    /**
     * @var BeardenCheckData
     * Holds the BeardenUser object
     */
    private $BeardenCheckData = NULL;
    /**
     * @varBeardenUser
     * Holds the BeardenUser object
     */
    private $BeardenUser = NULL;
    /**
     * @varBeardenRedirect
     * Holds the BeardenUser object
     */
    private $BeardenRedirect = NULL;
    /**
     * @var BeardenActionHandler
     * Holds the BeardenActionHandler
     */

    private $BeardenActionHandler = NULL;
    /**
     * @var BeardenMySQL
     * Holds the BeardenMySQL
     */
    private $BeardenMySQL = NULL;

    /**
     * Starts the session and set the values to the private objects
     */


    function __construct ()
    {

        /* Create a session when the object is initialized */

        session_start ();

    }

    /**
     * @return \BeardenCheckData
     */
    public function getBeardenCheckData ()
    {
        /* Check if Bearden Check Data is NULL */

        $this->BeardenCheckData == NULL

            /* If it is NULL, we need to include the object file */

            ? include_once "BeardenCheckData.php"

            /* It's already been set, now we just return it */

            : FALSE;

        /* Check if Bearden Check Data is NULL */

        $this->BeardenCheckData == NULL

            /* If it is NULL, we need to create a new Bearden Check Data object */

            ? $this->BeardenCheckData = new BeardenCheckData()

            /* It's already been set, now we just return it */

            : FALSE;

        /* Return the object */

        return $this->BeardenCheckData;
    }

    /**
     * @return BeardenMySQL
     */
    public function getBeardenMySQL ()
    {
        /* Check if Bearden MySQL is NULL */

        $this->BeardenMySQL == NULL

            /* If it is NULL, we need to include the object file */

            ? include_once "BeardenMySQL.php"

            /* It's already been set, now we just return it */

            : FALSE;


        /* Check if Bearden MySQL is NULL */

        $this->BeardenMySQL == NULL

            /* If it is NULL, we need to create a new Bearden MySQL object */

            ? $this->BeardenMySQL = new BeardenMySQL()

            /* It's already been set, now we just return it */

            : FALSE;

        /* Return the object */

        return $this->BeardenMySQL;

    }

    /**
     * @return \BeardenActionHandler
     */
    public function getBeardenActionHandler ()
    {

        /* Check if Bearden Action Handler is NULL */

        $this->BeardenActionHandler == NULL

            /* If it is NULL, we need to include the object file */

            ? include_once "BeardenActionHandler.php"

            /* It's already been set, now we just return it */

            : FALSE;


        /* Check if Bearden Action Handler is NULL */

        $this->BeardenActionHandler == NULL

            /* If it is NULL, we need to create a new BeardenActionHandler object */

            ? $this->BeardenActionHandler = new BeardenActionHandler()

            /* It's already been set, now we just return it */

            : FALSE;

        /* Return the object */

        return $this->BeardenActionHandler;
    }

    /**
     * @return \BeardenRedirect
     */
    public function getBeardenRedirect ()
    {
        /* Check if Bearden Redirect  is NULL */

        $this->BeardenRedirect == NULL

            /* If it is NULL, we need to include the object file */

            ? include_once "BeardenRedirect.php"

            /* It's already been set, now we just return it */

            : FALSE;

        /* Check if Bearden Redirect  is NULL */

        $this->BeardenRedirect == NULL

            /* If it is NULL, we need to create a new Bearden Redirect object */

            ? $this->BeardenRedirect = new BeardenRedirect()

            /* It's already been set, now we just return it */

            : FALSE;

        /* Return the object */

        return $this->BeardenRedirect;
    }

    /**
     * @return \BeardenUser
     */
    public function getBeardenUser ()
    {
        /* Check if Bearden User  is NULL */

        $this->BeardenUser == NULL

            /* If it is NULL, we need to include the object file */

            ? include_once "BeardenUser.php"

            /* It's already been set, now we just return it */

            : FALSE;

        /* Check if Bearden User  is NULL */

        $this->BeardenUser == NULL

            /* If it is NULL, we need to create a new Bearden Redirect object */

            ? $this->BeardenUser = new BeardenUser()

            /* It's already been set, now we just return it */

            : FALSE;

        /* Return the object */

        return $this->BeardenUser;
    }

    /**
     * Clearing out the variables to free up memory
     */
    function __destruct ()
    {

        /* Null out all the objects to clear memory */

        $this->BeardenActionHandler = NULL;

        $this->BeardenRedirect = NULL;

        $this->BeardenCheckData = NULL;

        $this->BeardenMySQL = NULL;
    }

}